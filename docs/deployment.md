# Grady deployment instructions

For every commit on master a Docker image is build which can be deployed
anywhere. The current deployment configurations is explained in another repo.

## Environment variables

- `GRADY_LOG_LEVEL` Sets the log level for our custom logging configuration
  (default: DEBUG).
- `GRADY_LOG_FORMAT` Can be set to `json` in order to make logs readable
  by Logstash (default: `default-format`).
