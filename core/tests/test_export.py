from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from util.factories import make_test_data, make_exams


def make_data():
    exams = make_exams([{
        'module_reference': 'Test Exam 01',
        'total_score': 100,
        'pass_score': 60,
        'pass_only': True
        }]
    )
    return make_test_data(data_dict={
        'exams': [{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
            'pass_only': True,
            'exam_type_id': exams[0].exam_type_id
        }],
        'submission_types': [
            {
                'name': '01. Sort',
                'full_score': 35,
                'description': 'Very complicated',
                'solution': 'Trivial!',
                'programming_language': 'Haskell',
                'exam_type': exams[0]
            },
            {
                'name': '02. Shuffle',
                'full_score': 35,
                'description': 'Very complicated',
                'solution': 'Trivial!',
                'exam_type': exams[0]
            }
        ],
        'students': [
            {'username': 'student01', 'exam': 'Test Exam 01'},
            {'username': 'student02', 'exam': 'Test Exam 01'}
        ],
        'tutors': [{
            'username': 'tutor01'
        }],
        'reviewers': [
            {'username': 'reviewer'}
        ],
        'submissions': [
            {
                'text': 'function blabl\n'
                        '   on multi lines\n'
                        '       for blabla in bla:\n'
                        '           lorem ipsum und so\n',
                'type': '01. Sort',
                'user': 'student01',
                'feedback': {
                    'score': 5,
                    'is_final': True,
                    'feedback_lines': {
                        '1': [{
                            'text': 'This is very bad!',
                            'of_tutor': 'reviewer'
                        }],
                    }

                }
            },
            {
                'text': 'not much',
                'type': '02. Shuffle',
                'user': 'student01'
            },
            {
                'text': 'function blabl\n'
                        '       asasxasx\n'
                        '           lorem ipsum und so\n',
                'type': '01. Sort',
                'user': 'student02'
            },
            {
                'text': 'not much to see here',
                'type': '02. Shuffle',
                'user': 'student02'
            }
        ]}
    )


class ExportInstanceTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.data = make_data()

    def setUp(self):
        self.client = APIClient()
        self.client.force_login(user=self.data['reviewers'][0])
        self.response = self.client.get('/api/instance/export/',
                                        data={'setPasswords': True,
                                              'selected_exam': self.data['exams'][0].exam_type_id})

    def test_can_access(self):
        self.assertEqual(status.HTTP_200_OK, self.response.status_code)

    def test_data_is_correct(self):
        instance = self.response.json()

        # examTypes fields
        self.assertIn('examTypes', instance)
        self.assertIn('pk', instance['examTypes'][0])
        self.assertEqual('Test Exam 01', instance['examTypes'][0]['moduleReference'])
        self.assertEqual(100, instance['examTypes'][0]['totalScore'])
        self.assertEqual(60, instance['examTypes'][0]['passScore'])
        self.assertEqual(True, instance['examTypes'][0]['passOnly'])

        # submissionTypes fields
        self.assertIn('submissionTypes', instance)
        self.assertEqual(2, len(instance['submissionTypes']))
        self.assertIn('pk', instance['submissionTypes'][0])
        self.assertEqual('01. Sort', instance['submissionTypes'][0]['name'])
        self.assertEqual(35, instance['submissionTypes'][0]['fullScore'])
        self.assertEqual('Very complicated', instance['submissionTypes'][0]['description'])
        self.assertEqual('Trivial!', instance['submissionTypes'][0]['solution'])
        self.assertEqual('Haskell', instance['submissionTypes'][0]['programmingLanguage'])

        # students fields
        self.assertIn('students', instance)
        self.assertEqual(2, len(instance['students']))
        self.assertIn('pk', instance['students'][0])
        self.assertIn('userPk', instance['students'][0])
        self.assertIn('exams', instance['students'][0])
        student_users = [s['user'] for s in instance['students']]
        self.assertIn('student01', student_users)
        self.assertIn('student02', student_users)
        self.assertLess(0, len(instance['students'][1]['submissions']))

        # students[submissions] nested
        self.assertIn('submissions', instance['students'][1])
        self.assertLess(0, len(instance['students'][1]['submissions']))
        self.assertIn('pk', instance['students'][1]['submissions'][0])
        self.assertIn('function blabl', instance['students'][1]['submissions'][0]['text'])
        self.assertIn('type', instance['students'][1]['submissions'][0])
        self.assertIn('tests', instance['students'][1]['submissions'][0])

        # students[submissions][feedback] nested
        submissions = instance['students'][0]['submissions']
        self.assertIn('feedback', submissions[0])
        self.assertLess(0, len(submissions[0]['feedback']))
        self.assertEqual(5, submissions[0]['feedback']['score'])
        self.assertEqual(True, submissions[0]['feedback']['isFinal'])
        self.assertIn('created', submissions[0]['feedback'])

        # students[submissions][feedback][feedbackLines] nested
        feedback = instance['students'][0]['submissions'][0]['feedback']
        self.assertIn('feedbackLines', feedback)
        self.assertLess(0, len(feedback['feedbackLines']))
        self.assertIn('1', feedback['feedbackLines'])
        self.assertIn('pk', feedback['feedbackLines']['1'][0])
        self.assertEqual('This is very bad!', feedback['feedbackLines']['1'][0]['text'])
        self.assertEqual('reviewer', feedback['feedbackLines']['1'][0]['ofTutor'])

        # reviewers fields
        self.assertIn('reviewers', instance)
        self.assertLess(0, len(instance['reviewers']))
        self.assertIn('pk', instance['reviewers'][0])
        self.assertEqual('reviewer', instance['reviewers'][0]['username'])

        # tutors fields
        self.assertIn('tutors', instance)
        self.assertLess(0, len(instance['tutors']))
        tutor_names = [t['username'] for t in instance['tutors']]
        self.assertIn('tutor01', tutor_names)
        self.assertIn('reviewer', tutor_names)


class ExportJSONTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.data = make_data()

    def setUp(self):
        self.client = APIClient()
        self.client.force_login(user=self.data['reviewers'][0])
        self.response = self.client.post('/api/export/json/',
                                         data={'selected_exam': self.data['exams'][0].exam_type_id})

    def test_can_access(self):
        self.assertEqual(status.HTTP_200_OK, self.response.status_code)

    def test_data_is_correct(self):
        # due to using the client, we need to parse the json
        student1, student2 = self.response.data
        self.assertIn('Matrikel', student1)
        self.assertIn('Matrikel', student2)

        self.assertEqual('', student1['Name'])
        self.assertEqual('', student2['Name'])

        self.assertEqual('Test Exam 01', student1['Exam'])
        self.assertEqual('Test Exam 01', student2['Exam'])

        self.assertEqual('student01', student1['Username'])
        self.assertEqual('student02', student2['Username'])

        self.assertEqual('********', student2['Password'])
        self.assertEqual('********', student1['Password'])

        self.assertEqual('01. Sort', student1['Scores'][0]['type'])
        self.assertEqual('01. Sort', student2['Scores'][0]['type'])

        self.assertEqual('02. Shuffle', student1['Scores'][1]['type'])
        self.assertEqual('02. Shuffle', student2['Scores'][1]['type'])

        self.assertEqual(5, student1['Scores'][0]['score'])
        self.assertEqual(0, student2['Scores'][0]['score'])

        self.assertEqual(0, student2['Scores'][1]['score'])
        self.assertEqual(0, student2['Scores'][1]['score'])


class ExportJSONAndSetPasswordsTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.data = make_data()

    def setUp(self):
        self.client = APIClient()
        self.client.force_login(user=self.data['reviewers'][0])
        self.response = self.client.post('/api/export/json/',
                                         data={'setPasswords': True,
                                               'selected_exam': self.data['exams'][0].exam_type_id})

    def test_can_access(self):
        self.assertEqual(status.HTTP_200_OK, self.response.status_code)

    def test_data_contains_correct_password(self):
        student1, student2 = self.response.data
        ret = self.client.login(username=student1['Username'], password=student1['Password'])
        self.assertTrue(ret)
        ret = self.client.login(username=student2['Username'], password=student2['Password'])
        self.assertTrue(ret)
