import pytest
import os

from rest_framework.test import APIClient, APITestCase
from constance.test import override_config
from core.models import UserAccount


class AuthTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.credentials = {'username': 'user', 'password': 'p'}
        cls.user = UserAccount.objects.create(
            username=cls.credentials['username'])
        cls.user.set_password(cls.credentials['password'])
        cls.user.save()
        cls.client = APIClient()

    def test_get_token(self):
        response = self.client.post('/api/get-token/', self.credentials)
        self.assertContains(response, 'token')

    def test_refresh_token(self):
        token = self.client.post('/api/get-token/', self.credentials).data
        response = self.client.post('/api/refresh-token/', token)
        self.assertContains(response, 'token')

    @override_config(REGISTRATION_PASSWORD='pw')
    def test_registration_correct_password(self):
        credentials = {
            'username': 'john-doe',
            'password': 'safeandsound',
            'registration_password': 'pw',
        }
        response = self.client.post('/api/corrector/register/', credentials)
        self.assertEqual(201, response.status_code)

    @override_config(REGISTRATION_PASSWORD='wrong_pw')
    def test_registration_wrong_password(self):
        credentials = {
            'username': 'john-doe',
            'password': 'safeandsound',
            'registration_password': 'pw',
        }
        response = self.client.post('/api/corrector/register/', credentials)
        self.assertEqual(403, response.status_code)

    @pytest.mark.skipif(os.environ.get('DJANGO_DEV', False),
                        reason="No password strengths checks in dev")
    @override_config(REGISTRATION_PASSWORD='pw')
    def test_password_is_strong_enough(self):
        response = self.client.post('/api/corrector/register/', {
            'username': 'hans',
            'password': 'weak',
            'registration_password': 'pw',
        })

        self.assertEqual(400, response.status_code)
        self.assertIn('password', response.data)

    @override_config(REGISTRATION_PASSWORD='pw')
    def test_cannot_register_active(self):
        response = self.client.post('/api/corrector/register/', {
            'username': 'hans',
            'password': 'safeandsound',
            'registration_password': 'pw',
            'is_active': True
        })

        self.assertEqual(403, response.status_code)
