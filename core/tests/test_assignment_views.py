from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from core import models
from core.models import (TutorSubmissionAssignment)
from util.factories import make_test_data, make_exams


class TestApiEndpoints(APITestCase):

    @classmethod
    def setUpTestData(cls):
        exams = make_exams([{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
        }]
        )
        cls.data = make_test_data(data_dict={
            'exams': [{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
                'exam_type_id': exams[0].exam_type_id
            }],
            'submission_types': [
                {
                    'name': '01. Sort this or that',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!',
                    'exam_type': exams[0]
                },
                {
                    'name': '02. Merge this or that or maybe even this',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!',
                    'exam_type': exams[0]
                }
            ],
            'students': [
                {
                    'username': 'student01',
                    'password': 'p',
                    'exam': 'Test Exam 01'
                },
                {
                    'username': 'student02',
                    'password': 'p',
                    'exam': 'Test Exam 01'
                }
            ],
            'tutors': [
                {'username': 'tutor01', 'password': 'p'},
                {'username': 'tutor02', 'password': 'p'}
            ],
            'reviewers': [
                {'username': 'reviewer', 'password': 'p'}
            ],
            'submissions': [
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student01',
                    'feedback': {
                        'score': 5,
                        'is_final': True,
                        'feedback_lines': {
                            '1': [{
                                'text': 'This is very bad!',
                                'of_tutor': 'tutor01'
                            }],
                        }

                    }
                },
                {
                    'text': 'function blabl\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student01'
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student02'
                },
                {
                    'text': 'function lorem ipsum etc\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student02'
                },
            ]}
        )

    def setUp(self):
        self.client = APIClient()

    def test_tutor_gets_an_assignment(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    # we should simply test if any newly created assignment is unfinished
    def test_first_work_assignment_was_created_unfinished(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertFalse(TutorSubmissionAssignment.objects.first().is_done)

    def test_assignment_raises_error_when_depleted(self):
        self.data['submissions'][0].delete()
        self.data['submissions'][2].delete()

        self.client.force_authenticate(user=self.data['tutors'][0])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_assignment_delete_of_done_not_permitted(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        first = TutorSubmissionAssignment.objects.first()
        first.is_done = True
        first.save()

        self.assertRaises(models.DeletionOfDoneAssignmentsNotPermitted,
                          first.delete)

    def test_assignment_delete_undone_permitted(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        first = TutorSubmissionAssignment.objects.first()
        first.delete()

        self.assertEqual(0, TutorSubmissionAssignment.objects.all().count())

    def tutor_can_release_own_unfinished_assignments(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.client.post(
            f'/api/assignment/{response.data["pk"]}/finish/', {
                "score": 23,
                "of_submission": response.data['submission']['pk'],
                "feedback_lines": {
                    1: {"text": "< some string >", "labels": []},
                    2: {"text": "< some string >", "labels": []}
                },
                "labels": [],
            }
        )
        self.assertEqual(2, TutorSubmissionAssignment.objects.all().count())
        self.assertEqual(1, TutorSubmissionAssignment.objects.filter(is_done=True).count())

        self.client.force_authenticate(user=self.data['tutors'][1])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        response = self.client.delete('/api/assignment/')
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(2, TutorSubmissionAssignment.objects.all().count())

    def test_two_tutors_cant_have_assignments_for_same_submission(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        assignment_fst_tutor = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][1].pk,
            "stage": "feedback-creation",
        }).data

        self.client.force_authenticate(user=self.data['tutors'][1])

        assignment_snd_tutor = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][1].pk,
            "stage": "feedback-creation",
        }).data

        self.assertNotEqual(assignment_fst_tutor['submission']['pk'],
                            assignment_snd_tutor['submission']['pk'])

    def test_reviewer_can_get_active_assignments(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        assignment = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        }).data

        # tutors shouldn't have access
        res = self.client.get('/api/assignment/active/')
        self.assertEqual(status.HTTP_403_FORBIDDEN, res.status_code)

        self.client.force_authenticate(user=self.data['reviewers'][0])

        active_assignments = self.client.get('/api/assignment/active/').data
        self.assertIn(assignment['pk'], [assignment['pk'] for assignment in active_assignments])

    def test_reviewer_can_delete_active_assignments(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        assignment = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        }).data

        # tutors shouldn't have access
        res = self.client.delete('/api/assignment/active/')
        self.assertEqual(status.HTTP_403_FORBIDDEN, res.status_code)

        self.client.force_authenticate(user=self.data['reviewers'][0])

        res = self.client.delete('/api/assignment/active/')
        self.assertEqual(status.HTTP_204_NO_CONTENT, res.status_code)
        self.assertNotIn(
            assignment['pk'],
            [assignment.pk for assignment
             in TutorSubmissionAssignment.objects.filter(is_done=False)]
        )

    def test_all_stages_of_the_subscription(self):
        self.client.force_authenticate(user=self.data['tutors'][0])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-creation",
        })
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        response = self.client.post(
            f'/api/assignment/{response.data["pk"]}/finish/', {
                "score": 23,
                "of_submission": response.data['submission']['pk'],
                "feedback_lines": {
                    1: {"text": "< some string >", "labels": []},
                    2: {"text": "< some string >", "labels": []}
                },
                "labels": [],
            }
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        # some other tutor reviews it
        self.client.force_authenticate(user=self.data['tutors'][1])

        response = self.client.post('/api/assignment/', {
            "submission_type": self.data['submission_types'][0].pk,
            "stage": "feedback-validation",
        })

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        submission_id_in_database = models.Feedback.objects.filter(
            is_final=False).first().of_submission.submission_id
        submission_id_in_response = response.data['submission']['pk']

        self.assertEqual(
            str(submission_id_in_database),
            submission_id_in_response)

        assignment = models.TutorSubmissionAssignment.objects.get(pk=response.data['pk'])
        self.assertFalse(assignment.is_done)
        response = self.client.post(
            f'/api/assignment/{assignment.pk}/finish/', {
                "score": 20,
                "is_final": True,
                "feedback_lines": {
                    2: {"text": "< some addition by second tutor>"},
                }
            }
        )

        assignment.refresh_from_db()
        meta = assignment.submission.meta
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(2, len(response.data['feedback_lines'][2]))
        self.assertTrue(assignment.is_done)
        self.assertIn(self.data['tutors'][0], meta.feedback_authors.all())
        self.assertIn(self.data['tutors'][1], meta.feedback_authors.all())
