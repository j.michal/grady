from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from core.models import UserAccount, SubmissionType

from util.factories import GradyUserFactory

test_data = {
    "meta": {
        "version": "6.0.0"
    },
    "data": {
        "module": {
            "module_reference": "test",
            "pass_only": True,
            "pass_score": 1,
            "total_score": 99
        },
        "students": [
            {
                "fullname": "test",
                "identifier": "test-test",
                "submissions": [
                    {
                        "code": "some messy, perhaps incorrect stuff",
                        "tests": [],
                        "type": "[a0] coding stuff"
                    },
                    {
                        "code": "i don't know man",
                        "tests": [],
                        "type": "[a1] improvise"
                    }
                ],
            }
        ],
        "submission_types": [
            {
                "description": "code some 1337 stuff",
                "full_score": 99,
                "name": "[a0] coding stuff",
                "programming_language": "c",
                "solution": "how dare u"
            },
            {
                "description": "now this one's hard",
                "full_score": 1,
                "name": "[a1] improvise",
                "programming_language": "haskell",
                "solution": "nope"
            },
        ]
    }
}


class ImportViewTest(APITestCase):

    factory = GradyUserFactory()

    def setUp(self):
        self.url = '/api/import/'
        self.client = APIClient()
        self.client.force_login(user=self.factory.make_reviewer())

    def test_can_not_submit_nothing(self):
        res = self.client.post(self.url)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res.status_code)

    def test_will_fail_on_wrong_importer_version(self):
        data = {"meta": {"version": "0.0.0"}}
        res = self.client.post(self.url, data)
        self.assertEqual(status.HTTP_409_CONFLICT, res.status_code)

    def test_data_is_imported_correctly(self):
        res = self.client.post(self.url, test_data)

        sub_types = SubmissionType.objects.all()
        students = UserAccount.objects.all().filter(role='Student')

        self.assertEqual(2, len(sub_types))
        self.assertEqual(1, len(students))
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)
