from rest_framework import status
from rest_framework.test import APITestCase

from core.models import FeedbackLabel
from util.factories import GradyUserFactory, make_exams


class LabelsTestCases(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.factory = GradyUserFactory()
        cls.exam = make_exams(exams=[{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
        }])[0]
        cls.student = cls.factory.make_student(exam=cls.exam)
        cls.tutor = cls.factory.make_tutor(exam=cls.exam)
        cls.reviewer = cls.factory.make_reviewer(exam=cls.exam)
        cls.label_post_data = {
            'name': 'A label',
            'description': 'with a description...'
        }
        cls.label_url = '/api/label/'

    def test_student_can_read_labels(self):
        self.client.force_authenticate(user=self.student)
        response = self.client.get(self.label_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(FeedbackLabel.objects.count(), 0)

    def test_student_can_not_write_labels(self):
        self.client.force_authenticate(user=self.student)
        response = self.client.post(self.label_url, data=self.label_post_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(FeedbackLabel.objects.count(), 0)

    def test_tutor_can_create_label(self):
        self.client.force_authenticate(user=self.tutor)
        response = self.client.post(self.label_url, data=self.label_post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FeedbackLabel.objects.count(), 1)

    def test_reviewer_can_create_label(self):
        self.client.force_authenticate(user=self.reviewer)
        response = self.client.post(self.label_url, data=self.label_post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FeedbackLabel.objects.count(), 1)
