import logging

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core import models
from core.serializers import (DynamicFieldsModelSerializer, CommentDictionarySerializer,
                              ExamSerializer)

log = logging.getLogger(__name__)


class SolutionCommentSerializer(DynamicFieldsModelSerializer):
    of_user = serializers.StringRelatedField(source='of_user.username')

    def validate(self, attrs):
        super().validate(attrs)
        submission_type = attrs.get('of_submission_type')
        of_line = attrs.get('of_line')
        if self.instance:
            submission_type = self.instance.of_submission_type
            of_line = self.instance.of_line

        max_line_number = len(submission_type.solution.split('\n'))

        if not (0 < of_line <= max_line_number):
            raise ValidationError('Invalid line number for comment')
        return attrs

    def create(self, validated_data):
        validated_data['of_user'] = self.context['request'].user
        return super().create(validated_data)

    class Meta:
        model = models.SolutionComment
        fields = (
            'pk',
            'text',
            'created',
            'of_user',
            'of_line',
            'of_submission_type'
        )
        read_only_fields = ('pk', 'created', 'of_user')
        list_serializer_class = CommentDictionarySerializer


class SubmissionTypeListSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = models.SubmissionType
        fields = ('pk', 'name', 'full_score')


class SubmissionTypeSerializer(DynamicFieldsModelSerializer):
    solution_comments = SolutionCommentSerializer(many=True, required=False)
    exam_type = ExamSerializer()

    class Meta:
        model = models.SubmissionType
        fields = ('pk',
                  'name',
                  'exam_type',
                  'full_score',
                  'description',
                  'solution',
                  'programming_language',
                  'solution_comments')
