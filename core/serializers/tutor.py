import logging

import django.contrib.auth.password_validation as validators
from django.core import exceptions
from rest_framework import serializers

from core import models
from util.factories import GradyUserFactory

from .generic import DynamicFieldsModelSerializer

log = logging.getLogger(__name__)
user_factory = GradyUserFactory()


class CorrectorSerializer(DynamicFieldsModelSerializer):
    feedback_created = serializers.SerializerMethodField()
    feedback_validated = serializers.SerializerMethodField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True,
        required=False
    )
    role = serializers.CharField(read_only=True)

    def get_feedback_created(self, t):
        ''' It is required that this field was previously annotated '''
        return t.feedback_created if hasattr(t, 'feedback_created') else 0

    def get_feedback_validated(self, t):
        ''' It is required that this field was previously annotated '''
        return t.feedback_validated if hasattr(t, 'feedback_validated') else 0

    def create(self, validated_data) -> models.UserAccount:
        log_validated_data = dict(validated_data)
        log_validated_data['password'] = '******'
        log.info("Crating tutor from data %s", log_validated_data)
        return user_factory.make_tutor(
            username=validated_data['username'],
            password=validated_data.get('password'),
            is_active=validated_data.get('is_active', False))

    def validate(self, data):
        user = models.UserAccount(**data)
        password = data.get('password')

        try:
            if password is not None:
                validators.validate_password(password=password, user=user)
        except exceptions.ValidationError as err:
            raise serializers.ValidationError({'password': list(err.messages)})
        return data

    class Meta:
        model = models.UserAccount
        fields = ('pk',
                  'password',
                  'is_active',
                  'username',
                  'feedback_created',
                  'feedback_validated',
                  'exercise_groups',
                  'role')
