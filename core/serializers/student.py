from rest_framework import serializers

from core.models import StudentInfo, ExamInfo
from core.serializers import DynamicFieldsModelSerializer, ExamSerializer
from core.serializers.submission import (SubmissionListSerializer,
                                         SubmissionNoTextFieldsSerializer,
                                         SubmissionNoTypeSerializer)


class ExamInfoListSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = ExamInfo
        fields = ('exam', 'student', 'total_score', 'passes_exam')


class StudentInfoSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    matrikel_no = serializers.ReadOnlyField(source='user.matrikel_no')
    exams = ExamInfoListSerializer(many=True)
    submissions = SubmissionListSerializer(many=True)

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'matrikel_no',
                  'submissions',
                  'exams')


class StudentInfoForListViewSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    user = serializers.ReadOnlyField(source='user.username')
    user_pk = serializers.ReadOnlyField(source='user.pk')
    exams = serializers.ReadOnlyField(source='exams.module_reference')
    submissions = SubmissionNoTextFieldsSerializer(many=True)
    is_active = serializers.BooleanField(source='user.is_active')

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'user_pk',
                  'exams',
                  'submissions',
                  'matrikel_no',
                  'is_active')


class StudentExportSerializer(DynamicFieldsModelSerializer):
    name = serializers.ReadOnlyField(source='user.fullname')
    user = serializers.ReadOnlyField(source='user.username')
    user_pk = serializers.ReadOnlyField(source='user.pk')
    exams = ExamInfoListSerializer(many=True)
    email = serializers.ReadOnlyField(source='user.email')
    is_active = serializers.BooleanField(source='user.is_active')
    submissions = SubmissionNoTypeSerializer(many=True)

    class Meta:
        model = StudentInfo
        fields = ('pk',
                  'name',
                  'user',
                  'user_pk',
                  'exams',
                  'email',
                  'submissions',
                  'matrikel_no',
                  'is_active')


class ExamInfoSerializer(DynamicFieldsModelSerializer):
    exam = ExamSerializer()
    student = StudentInfoSerializer()

    class Meta:
        model = ExamInfo
        fields = ('exam', 'student', 'total_score', 'passes_exam')
