# Generated by Django 3.1.7 on 2022-05-18 08:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20210902_1140'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='exam',
        ),
    ]
