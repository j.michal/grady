# Generated by Django 2.2.12 on 2020-09-22 11:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20200922_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examinfo',
            name='exam',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exam_infos', to='core.ExamType'),
        ),
    ]
