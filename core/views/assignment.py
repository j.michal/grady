import logging

from rest_framework import mixins, status, viewsets
from rest_framework import decorators
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from core import models, serializers
from core.models import TutorSubmissionAssignment
from core.permissions import IsReviewer, IsTutorOrReviewer
from core.serializers import AssignmentDetailSerializer, AssignmentSerializer

from multiprocessing import Lock

from core.views.util import tutor_attempts_to_patch_first_feedback_final

log = logging.getLogger(__name__)


class AssignmentApiViewSet(
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    serializer_class = AssignmentSerializer
    permission_classes = (IsTutorOrReviewer, )

    def get_queryset(self):
        base_queryset = TutorSubmissionAssignment.objects.all()
        if self.action in ['list', 'active', 'destroy']:
            return base_queryset.all()
        else:
            return base_queryset.filter(owner=self.request.user)

    def _fetch_assignment(self, serializer):
        try:
            serializer.save()
        except models.SubmissionTypeDepleted as err:
            return Response({'Error': str(err)},
                            status=status.HTTP_404_NOT_FOUND)
        except models.NotMoreThanTwoOpenAssignmentsAllowed as err:
            return Response({'Error': str(err)},
                            status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @decorators.permission_classes((IsReviewer,))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @decorators.action(detail=False, permission_classes=(IsReviewer,), methods=['get', 'delete'])
    def active(self, request):
        if request.method == 'GET':
            queryset = self.get_queryset().filter(is_done=False)
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        else:
            self.get_queryset().filter(is_done=False).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    @decorators.action(detail=False, permission_classes=(IsTutorOrReviewer,), methods=['delete'])
    def release(self, request):
        self.get_queryset().filter(
            is_done=False
        ).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @decorators.action(detail=True, methods=['post'])
    def finish(self, request, *args, **kwargs):
        context = self.get_serializer_context()
        instance = self.get_object()
        if instance.is_done or (instance.owner != request.user):
            return Response(status=status.HTTP_403_FORBIDDEN)
        try:
            orig_feedback = instance.submission.feedback
            serializer = serializers.FeedbackSerializer(
                orig_feedback,
                data=request.data,
                context=context,
                partial=True)
            if orig_feedback.final_by_reviewer and request.user.role == models.UserAccount.TUTOR:
                raise PermissionDenied(detail="Unfortunately you won't be able to finish this"
                                              "assignment since a reviewer has marked it as "
                                              "final while you were assigned.")
        except models.Feedback.DoesNotExist:
            serializer = serializers.FeedbackSerializer(
                data=request.data,
                context=context)

        serializer.is_valid(raise_exception=True)
        if tutor_attempts_to_patch_first_feedback_final(serializer, self.request.user, instance):
            raise PermissionDenied(
                detail='Cannot set the first feedback final.')
        serializer.save()
        instance.finish()
        response_status = status.HTTP_201_CREATED if \
            instance.stage == \
            models.TutorSubmissionAssignment.FEEDBACK_CREATION else status.HTTP_200_OK
        return Response(serializer.data, status=response_status)

    def destroy(self, request, pk=None):
        """ Stop working on the assignment before it is finished """
        instance = self.get_object()

        if instance.is_done or (instance.owner != request.user and
                                not request.user.is_reviewer()):
            return Response(status=status.HTTP_403_FORBIDDEN)

        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create(self, request, *args, **kwargs):
        with Lock():
            context = self.get_serializer_context()
            data = request.data
            serializer = AssignmentDetailSerializer(data=data,
                                                    context=context)
            serializer.is_valid(raise_exception=True)
            assignment = self._fetch_assignment(serializer)

        return assignment

    def retrieve(self, request, *args, **kwargs):
        assignment = self.get_object()
        if assignment.owner != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = AssignmentDetailSerializer(assignment)
        return Response(serializer.data)
