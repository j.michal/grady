import logging

from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver

from core import models
from core.models import (Feedback, FeedbackComment, MetaSubmission, Submission,
                         TutorSubmissionAssignment)

log = logging.getLogger(__name__)


@receiver(post_save, sender=Submission)
def create_meta_after_submission_create(sender, instance, created, **kwargs):
    log.debug('SIGNAL -- create_meta_after_submission_create')
    if created:
        MetaSubmission.objects.create(submission=instance)


@receiver(post_save, sender=TutorSubmissionAssignment)
def update_active_after_assignment_save(sender, instance, created, **kwargs):
    """ Assignments are created undone therefore save that no other
    should use it. If it is already set to done it is not active.
    """
    log.debug('SIGNAL -- update_active_after_assignment_save')
    meta = instance.submission.meta
    meta.has_active_assignment = created and not instance.is_done
    meta.save()


@receiver(pre_delete, sender=TutorSubmissionAssignment)
def remove_active_assignment_on_delete(sender, instance, **kwargs):
    log.debug('SIGNAL -- remove_active_assignment_on_delete')
    if instance.is_done:
        raise models.DeletionOfDoneAssignmentsNotPermitted()
    meta = instance.submission.meta
    meta.has_active_assignment = False
    meta.save()


@receiver(post_save, sender=Feedback)
def update_after_feedback_save(sender, instance, created, **kwargs):
    """ Do the following steps when feedback is saved:

    - set that feedback exists
    - copy the final status of the feedback
    - set all assignments of the submission done and remove active status
    """
    log.debug('SIGNAL -- update_after_feedback_save')
    meta = instance.of_submission.meta
    meta.has_feedback = True
    meta.has_final_feedback = instance.is_final or instance.final_by_reviewer
    meta.save()


@receiver(post_save, sender=Feedback)
def update_student_score(sender, instance, **kwargs):
    student = instance.of_submission.student

    for exam_info in student.exams.all():
        exam_info.update_total_score()
    log.debug('SIGNAL -- Scores of student %s were updated)', student)


@receiver(pre_save, sender=FeedbackComment)
def set_comment_visibility_after_conflict(sender, instance, **kwargs):
    log.debug('SIGNAL -- set_comment_visibility_after_conflict')
    comments_on_the_same_line = FeedbackComment.objects.filter(
        of_line=instance.of_line,
        of_feedback=instance.of_feedback,
    )
    comments_on_the_same_line.update(visible_to_student=False)
    instance.visible_to_student = True
