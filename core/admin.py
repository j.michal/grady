from django.contrib import admin
from django.contrib.auth.models import Group

from core.models import (ExamType, Feedback, StudentInfo, Submission,
                         SubmissionType, Test, UserAccount)

# Stuff we needwant
admin.site.register(UserAccount)
admin.site.register(SubmissionType)
admin.site.register(Feedback)
admin.site.register(Test)
admin.site.register(ExamType)
admin.site.register(Submission)
admin.site.register(StudentInfo)

# ... and stuff we don't needwant
admin.site.unregister(Group)
