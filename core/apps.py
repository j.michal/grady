from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    verbose_name = 'where everything comes together'

    def ready(self):
        import core.signals  # noqa
