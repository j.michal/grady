from django.urls import path, re_path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.routers import DefaultRouter
from rest_framework.permissions import AllowAny

from core import views

# Create a router and register our viewsets with it.

router = DefaultRouter()
router.register('student', views.StudentReviewerApiViewSet,
                basename='student')
router.register('examtype', views.ExamApiViewSet, basename='examtype')
router.register('feedback', views.FeedbackApiView, basename='feedback')
router.register('feedback-comment', views.FeedbackCommentApiView, basename='feedback-comment')
router.register('submission', views.SubmissionViewSet,
                basename='submission')
router.register('submissiontype', views.SubmissionTypeApiView, basename='submissiontype')
router.register('corrector', views.CorrectorApiViewSet, basename='corrector')
router.register('assignment', views.AssignmentApiViewSet, basename='assignment')
router.register('statistics', views.StatisticsEndpoint, basename='statistics')
router.register('user', views.UserAccountViewSet, basename='user')
router.register('label', views.LabelApiViewSet, basename='label')
router.register('label-statistics', views.LabelStatistics, basename='label-statistics')
router.register('solution-comment', views.SolutionCommentApiViewSet, basename='solution-comment')
router.register('group', views.GroupApiViewSet, basename='group')
router.register('config', views.InstanceConfigurationViewSet, basename='config')

schema_view = get_schema_view(
    openapi.Info(
        title="Grady API",
        default_version='v1',
        description="Blub",
    ),
    # validators=['flex', 'ssv'],
    public=True,
    permission_classes=(AllowAny,),
)

# regular views that are not viewsets
regular_views_urlpatterns = [
    path('student-page/',
         views.StudentSelfApiView.as_view(),
         name='student-page'),
    path('student-submissions/',
         views.StudentSelfSubmissionsApiView.as_view(),
         name='student-submissions'),
    path('instance/export/', views.InstanceExport.as_view(), name="instance-export"),
    path('export/json/', views.StudentJSONExport.as_view(), name='export-json'),
    path('import/', views.ImportApiViewSet.as_view(), name='import-json'),
    re_path(r'swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'swagger/$', schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'),
    re_path(r'redoc/$', schema_view.with_ui('redoc', cache_timeout=0),
            name='schema-redoc'),
]

urlpatterns = [
    *router.urls,
    *regular_views_urlpatterns,
]
