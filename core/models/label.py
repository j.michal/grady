import logging

from django.core.validators import RegexValidator
from django.db import models

from core.models.feedback import Feedback, FeedbackComment

log = logging.getLogger(__name__)

HexColourValidator = RegexValidator(
    regex='^#[0-9A-F]{6}$',
    message='Colour must be in format: #[0-9A-F]{7}',
    code='nomatch')


class FeedbackLabel(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField()
    colour = models.CharField(validators=[HexColourValidator], max_length=7, default='#b0b0b0')
    feedback = models.ManyToManyField(Feedback, related_name='labels')
    feedback_comments = models.ManyToManyField(FeedbackComment, related_name='labels')
