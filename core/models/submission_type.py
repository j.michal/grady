import logging
import uuid

import constance
from django.db import models
from django.db.models import (Case, Count, IntegerField, Q,
                              Value, When)
from django.db.models.query import QuerySet
from core.models.exam_type import ExamType


log = logging.getLogger(__name__)
config = constance.config


class SubmissionType(models.Model):
    """This model mostly holds meta information about the kind of task that was
    presented to the student. It serves as a foreign key for the submissions
    that are of this type. This model is currently NOT exposed directly in a
    view.

    Attributes
    ----------
    description : TextField
        The task description the student had to fulfill. The content may be
        HTML formatted.
    full_score : PositiveIntegerField
        Maximum score one can get on that one
    name : CharField
        The original title of the exam. This is wildly used as an identifier by
        the preprocessing scripts.
    solution : TextField
        A sample solution or a correction guideline
    """

    C = 'c'
    JAVA = 'java'
    MIPS = 'mipsasm'
    HASKELL = 'haskell'
    TEXT = 'plaintext'
    PYTHON = 'python'
    MARKDOWN = 'markdown'

    LANGUAGE_CHOICES = (
        (C, 'C syntax highlighting'),
        (JAVA, 'Java syntax highlighting'),
        (MIPS, 'Mips syntax highlighting'),
        (HASKELL, 'Haskell syntax highlighting'),
        (PYTHON, 'Python syntax highlighting'),
        (MARKDOWN, 'Markdown syntax highlighting with asciimath rendering'),
        (TEXT, 'No syntax highlighting'),
    )

    submission_type_id = models.UUIDField(primary_key=True,
                                          default=uuid.uuid4,
                                          editable=False)
    name = models.CharField(max_length=100, unique=True)
    full_score = models.PositiveIntegerField(default=0)
    description = models.TextField()
    solution = models.TextField()
    exam_type = models.ForeignKey(ExamType,
                                  on_delete=models.CASCADE,
                                  related_name='submission_types',
                                  null=True)
    programming_language = models.CharField(max_length=25,
                                            choices=LANGUAGE_CHOICES,
                                            default=C)

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name = "SubmissionType"
        verbose_name_plural = "SubmissionType Set"

    @classmethod
    def get_annotated_feedback_count(cls) -> QuerySet:
        """ Annotates submission lists with counts

        The following fields are annotated:
            * number of submissions per submission type
            * count of received *accepted* feedback per submission type
            * and finally the progress on each submission type as percentage

        The QuerySet that is return is ordered by name lexicographically.

        Returns:
            The annotated QuerySet as described above
        """
        return cls.objects\
            .annotate(  # to display only manual
                feedback_final=Count(
                    Case(When(
                        Q(submissions__meta__has_final_feedback=True),
                        then=Value(1)), output_field=IntegerField())
                ),
                feedback_in_validation=Count(
                    Case(When(
                        Q(submissions__meta__done_assignments=1) &
                        Q(submissions__meta__has_final_feedback=False),
                        then=Value(1)), output_field=IntegerField())
                ),
                feedback_in_conflict=Count(
                    Case(When(
                        Q(submissions__meta__done_assignments=2) &
                        Q(submissions__meta__has_final_feedback=False),
                        then=Value(1)), output_field=IntegerField())
                ),
                submission_count=Count('submissions'),
            ).order_by('name')


class SolutionComment(models.Model):
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    of_line = models.PositiveIntegerField()
    of_user = models.ForeignKey(
        'UserAccount',
        related_name="solution_comments",
        on_delete=models.PROTECT
    )
    of_submission_type = models.ForeignKey(
        SubmissionType,
        related_name="solution_comments",
        on_delete=models.PROTECT,
    )
