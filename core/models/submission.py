import logging
import uuid

import constance
from django.contrib.auth import get_user_model
from django.db import models

from core.models.submission_type import SubmissionType

log = logging.getLogger(__name__)
config = constance.config


class Submission(models.Model):
    """The answer of a student to a specific question. Holds the answer and
    very often serves as ForeignKey.

    With the method assign_tutor feedback for a submission can be created and a
    tutor will be assigned to this feedback permanently (unless deleted by a
    reviewer or if it gets reassigned). There cannot be more than ONE feedback
    per Submission.

    Attributes
    ----------
    seen_by_student : BooleanField
        True if the student saw his accepted feedback.
    student : ForgeignKey
        The student how cause all of this
    text : TextField
        The code/text submitted by the student
    source_code : TextField
        If the original "source code" is not easily displayable (like Jupyter Notebooks),
        it's converted to readable source code and the original is saved in this field
    source_code_available : BooleanField
        Whether there is original source code available under `source_code` or not
    type : OneToOneField
        Relation to the type containing meta information
    """
    submission_id = models.UUIDField(primary_key=True,
                                     default=uuid.uuid4,
                                     editable=False)
    seen_by_student = models.BooleanField(default=False)
    text = models.TextField(blank=True)
    source_code = models.TextField(null=True, blank=True, editable=False)
    source_code_available = models.BooleanField(default=False, editable=False)
    type = models.ForeignKey(
        SubmissionType,
        on_delete=models.PROTECT,
        related_name='submissions')
    student = models.ForeignKey(
        'StudentInfo',
        on_delete=models.CASCADE,
        related_name='submissions')

    class Meta:
        verbose_name = "Submission"
        verbose_name_plural = "Submission Set"
        unique_together = (('type', 'student'),)
        ordering = ('type__name',)

    def __str__(self) -> str:
        return "Submission {}".format(self.pk)


class MetaSubmission(models.Model):

    submission = models.OneToOneField('submission',
                                      related_name='meta',
                                      on_delete=models.CASCADE)
    done_assignments = models.PositiveIntegerField(default=0)
    has_active_assignment = models.BooleanField(default=False)

    # Managed by signal!
    has_feedback = models.BooleanField(default=False)
    # Managed by signal!
    has_final_feedback = models.BooleanField(default=False)

    feedback_authors = models.ManyToManyField(get_user_model())

    def __str__(self):
        return f''' Submission Meta of {self.submission}

        done_assignments      = {self.done_assignments}
        has_active_assignment = {self.has_active_assignment}
        has_feedback          = {self.has_feedback}
        has_final_feedback    = {self.has_final_feedback}
        feedback_authors      = {self.feedback_authors.values_list('username',
                                                                   flat=True)}
        '''
