import logging
import uuid
from collections import OrderedDict
from random import randrange
from typing import Dict

import constance
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import BooleanField, F, When, Sum, QuerySet, Value, Case
from django.db.models.functions import Coalesce

from core.models.submission_type import SubmissionType
from core.models.exam_type import ExamType

log = logging.getLogger(__name__)
config = constance.config


def random_matrikel_no() -> str:
    """Use as a default value for student's matriculation number.

    Returns:
        str: an eight digit number
    """
    return str(10_000_000 + randrange(90_000_000))


class ExamInfo(models.Model):
    exam = models.ForeignKey(ExamType,
                             on_delete=models.CASCADE,
                             related_name='exam_infos',
                             null=False)

    student = models.ForeignKey('StudentInfo',
                                on_delete=models.CASCADE,
                                related_name='exams',
                                null=False)

    total_score = models.PositiveIntegerField(default=0)
    passes_exam = models.BooleanField(default=False)

    def update_total_score(self):
        ''' This helper is invoked after feedback changes '''
        self.total_score = self.student.submissions.aggregate(
            Sum('feedback__score'))['feedback__score__sum'] or 0
        if self.exam is not None:
            self.passes_exam = self.total_score >= self.exam.pass_score
        self.save()

    def score_per_submission(self) -> Dict[str, int]:
        """ TODO: get rid of it and use an annotation. """
        if self.student.submissions.all():
            return OrderedDict({
                s.type.name: s.feedback.score if hasattr(s, 'feedback') else 0
                for s in self.student.submissions.filter(type__exam_type=self.exam)
                .order_by('type__name')
            })

        return OrderedDict({
            t.name: 0 for t in SubmissionType.objects.all()
        })


class StudentInfo(models.Model):
    """
    The StudentInfo model includes all information of a student, that we got
    from the E-Learning output, along with some useful classmethods that
    provide specially annotated QuerySets.

    Information like email (if given), and the username are stored in the
    associated user model.

    Attributes:
        exams (ManyToManyField):
            Module the student wants te be graded in, or different exercise
            assignments for one module.

        has_logged_in (BooleanField):
            Login is permitted once. If this is set the user can not log in.

        matrikel_no (CharField):
            The matriculation number of the student
    """
    student_id = models.UUIDField(primary_key=True,
                                  default=uuid.uuid4,
                                  editable=False)
    has_logged_in = models.BooleanField(default=False)
    matrikel_no = models.CharField(unique=True,
                                   max_length=30,
                                   default=random_matrikel_no)

    user = models.OneToOneField(get_user_model(),
                                on_delete=models.CASCADE,
                                related_name='student')

    def add_exam(self, exam):
        exam_info = ExamInfo(exam=exam, student=self)
        exam_info.save()
        self.exams.add(exam_info)

    @classmethod
    def get_annotated_score_submission_list(cls) -> QuerySet:
        """Can be used to quickly annotate a user with the necessary
        information on the overall score of a student and if he does not need
        any more correction.

        A student is done if
            * module type was pass_only and student has enough points
            * every submission got accepted feedback

        Returns
        -------
        QuerySet
            the annotated QuerySet as described above.
        """
        return cls.objects.annotate(
            overall_score=Coalesce(Sum('submissions__feedback__score'),
                                   Value(0)),
        ).annotate(
            done=Case(
                When(exam__pass_score__lt=F('overall_score'), then=Value(1)),
                default=Value(0),
                output_field=BooleanField()
            )
        ).order_by('user__username')

    def disable(self):
        """The student won't be able to login in anymore, but his current
        session can be continued until s/he logs out.
        """
        self.has_logged_in = True
        self.save()

    def __str__(self) -> str:
        return self.user.username

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Student Set"
