from django.db import models
from core.models.exam_type import ExamType
import uuid


class Group(models.Model):
    group_id = models.UUIDField(primary_key=True,
                                default=uuid.uuid4,
                                editable=False)
    name = models.CharField(max_length=120)
    exam = models.ForeignKey(ExamType,
                             on_delete=models.CASCADE,
                             related_name='groups',
                             null=True)
