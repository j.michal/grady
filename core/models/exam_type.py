
import logging
import uuid

import constance
from django.db import models

log = logging.getLogger(__name__)
config = constance.config


class ExamType(models.Model):
    """A model that contains information about the module a submission can
    belong to. The information is not needed and is currently, just used to
    detect if students already have enough points to pass an exam.

    It is NOT intended to use this for including different exams regarding
    submissions types.

    Attributes
    ----------
    module_reference : CharField
        a unique reference that identifies a module within the university
    pass_only : BooleanField
        True if no grade is given
    pass_score : PositiveIntegerField
        minimum score for (just) passing
    total_score : PositiveIntegerField
        maximum score for the exam (currently never used anywhere)
    """
    class Meta:
        verbose_name = "ExamType"
        verbose_name_plural = "ExamTypes"

    def __str__(self) -> str:
        return self.module_reference

    exam_type_id = models.UUIDField(primary_key=True,
                                    default=uuid.uuid4,
                                    editable=False)
    module_reference = models.CharField(max_length=50, unique=True)
    total_score = models.PositiveIntegerField()
    pass_score = models.PositiveIntegerField()
    pass_only = models.BooleanField(default=False)
