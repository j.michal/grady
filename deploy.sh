#!/bin/sh
sleep 5
python manage.py migrate --noinput
if [ "$?" -ne "0" ]; then
  exit 1
fi
gunicorn \
  --bind 0.0.0.0:8000 \
  --workers=5 \
  --timeout=120 \
  --worker-class=sync \
  --log-level debug \
  grady.wsgi:application
