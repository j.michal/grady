APP_LIST ?= core grady util
DB_NAME = postgres

.ONESHELL:

.PHONY: run install migrations-check isort isort-check test teste2e

run:
	python manage.py runserver 0.0.0.0:8000

migrations-check:
	python manage.py makemigrations --check --dry-run

isort:
	isort -rc $(APP_LIST)

isort-check:
	isort -c -rc $(APP_LIST)

migrate:
	python manage.py migrate

test:
	pytest --ds=grady.settings core/tests

frontend/dist: $(shell find frontend/src -type f)
	yarn --cwd frontend build

teste2e: frontend/dist
	set -e
	cp frontend/dist/index.html core/templates
	trap "git checkout core/templates/index.html" EXIT
	python util/format_index.py
	python manage.py collectstatic --no-input
	HEADLESS_TESTS=$(headless) pytest  --ds=grady.settings $(path)

coverage:
	set -e
	DJANGO_SETTINGS_MODULE=grady.settings pytest --cov
	coverage html

db:
	docker run -d --name $(DB_NAME) -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres:13
