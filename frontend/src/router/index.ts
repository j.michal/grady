import Vue from 'vue'
import Router, { RawLocation, Route, NavigationGuard } from 'vue-router'
import Login from '@/pages/Login.vue'
import ExamSelection from '@/pages/ExamSelectionPage.vue'
import StudentSubmissionPage from '@/pages/student/StudentSubmissionPage.vue'
import StudentOverviewPage from '@/pages/reviewer/StudentOverviewPage.vue'
import TutorOverviewPage from '@/pages/reviewer/TutorOverviewPage.vue'
import SubscriptionWorkPage from '@/pages/SubscriptionWorkPage.vue'
import SubscriptionEnded from '@/components/subscriptions/SubscriptionEnded.vue'
import PageNotFound from '@/pages/PageNotFound.vue'
import StartPageSelector from '@/pages/StartPageSelector.vue'
import Statistics from '@/pages/Statistics.vue'
import LayoutSelector from '@/pages/LayoutSelector.vue'
import StudentSubmissionSideView from '@/pages/StudentSubmissionSideView.vue'
import StudentListHelpCard from '@/components/student_list/StudentListHelpCard.vue'
import FeedbackHistoryPage from '@/pages/base/FeedbackHistoryPage.vue'
import FeedbackTable from '@/components/feedback_list/FeedbackTable.vue'
import FeedbackListHelpCard from '@/components/feedback_list/FeedbackListHelpCard.vue'
import VueInstance from '@/main'
import { Authentication } from '@/store/modules/authentication'
import { ConfigModule } from '@/store/modules/config'

Vue.use(Router)

type rerouteFunc = (to?: RawLocation | false | ((vm: Vue) => any) | void) => void

function denyAccess (next: rerouteFunc, redirect: Route) {
  next(redirect.path)
  VueInstance.$notify({
    title: 'Access denied',
    text: 'You don\'t have permission to view this.',
    type: 'error'
  })
}

let tutorOrReviewerOnly: NavigationGuard = function (to, from, next) {
  if (Authentication.isTutorOrReviewer) {
    next()
  } else {
    denyAccess(next, from)
  }
}

let reviewerOnly: NavigationGuard = function (to, from, next) {
  if (Authentication.isReviewer) {
    next()
  } else {
    denyAccess(next, from)
  }
}

const reviewerOrTutorInExerciseMode: NavigationGuard = function (to, from, next) {
  if (Authentication.isReviewer) {
    next()
  } else if (Authentication.isTutor && ConfigModule.state.config.instanceSettings.exerciseMode) {
    next()
  } else {
    denyAccess(next, from)
  }
}

let studentOnly: NavigationGuard = function (to, from, next) {
  if (Authentication.isStudent) {
    next()
  } else {
    next(false)
  }
}

let checkLoggedIn: NavigationGuard = function (to, from, next) {
  if (Authentication.isLoggedIn) {
    next()
  } else {
    next('/login/')
  }
}

const router = new Router({
  routes: [
    {
      path: '/login/',
      name: 'login',
      component: Login
    },
    {
      path: '/exam_selection/',
      name: 'exam-selection',
      beforeEnter: checkLoggedIn,
      component: ExamSelection
    },
    {
      path: '',
      redirect: 'home',
      beforeEnter: checkLoggedIn,
      component: LayoutSelector,
      children: [
        {
          path: 'home',
          name: 'home',
          component: StartPageSelector
        },
        {
          path: 'correction/:sub_type/:stage/:group?',
          name: 'correction',
          beforeEnter: tutorOrReviewerOnly,
          component: SubscriptionWorkPage
        },
        {
          path: 'correction/ended',
          name: 'correction-ended',
          component: SubscriptionEnded
        },
        {
          path: 'statistics',
          name: 'statistics',
          component: Statistics
        },
        {
          path: 'feedback',
          beforeEnter: tutorOrReviewerOnly,
          component: FeedbackHistoryPage,
          children: [
            {
              path: '',
              name: 'feedback',
              components: {
                left: FeedbackTable,
                right: FeedbackListHelpCard
              }
            },
            {
              path: ':submissionPk',
              components: {
                left: FeedbackTable,
                right: StudentSubmissionSideView
              },
              meta: {
                submissionSideView: true
              }
            },
            {
              path: ':id/detail',
              components: {
                left: FeedbackTable,
                right: PageNotFound
              }
            }
          ]
        },
        {
          path: 'participant-overview',
          beforeEnter: reviewerOrTutorInExerciseMode,
          component: StudentOverviewPage,
          children: [
            {
              path: '',
              name: 'participant-overview',
              component: StudentListHelpCard
            },
            {
              path: 'participant/:studentPk/submission/:submissionPk',
              name: 'submission-side-view',
              component: StudentSubmissionSideView,
              meta: {
                submissionSideView: true
              }
            }
          ]
        },
        {
          path: 'tutor-overview',
          name: 'tutor-overview',
          beforeEnter: reviewerOnly,
          component: TutorOverviewPage
        },
        {
          path: 'submission/:id',
          beforeEnter: studentOnly,
          component: StudentSubmissionPage
        }
      ]
    },
    {
      path: '*',
      name: 'page-not-found',
      component: PageNotFound
    }
  ]
})

export default router
