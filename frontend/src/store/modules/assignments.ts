import Vue from 'vue'
import * as api from '@/api'
import { cartesian, flatten, once } from '@/util/helpers'
import { Assignment, FeedbackStageEnum, CreateAssignment, AvailableSubmissionCounts, Group} from '@/models'
import { RootState } from '@/store/store'
import { Authentication } from '@/store/modules/authentication'
import { getStoreBuilder, BareActionContext } from 'vuex-typex'
import router from '@/router'
import { Route } from 'vue-router'

export interface AssignmentsState {
  currentAssignment?: Assignment
  assignmentCreation: {
    submissionType?: string
    stage: FeedbackStageEnum
    group?: Group
  },
  submissionsLeft: AvailableSubmissionCounts,
  groups: Group[],
  loading: boolean
}

function initialState (): AssignmentsState {
  return {
    currentAssignment: undefined,
    loading: false,
    assignmentCreation: {
      stage: FeedbackStageEnum.Creation,
      group: undefined,
      submissionType: undefined
    },
    submissionsLeft: {},
    groups: []
  }
}

const mb = getStoreBuilder<RootState>().module('Assignments', initialState())

const stateGetter = mb.state()


const availableStagesGetter = mb.read(function availableStages (state, getters) {
  let stages = [FeedbackStageEnum.Creation, FeedbackStageEnum.Validation]
  if (Authentication.isReviewer) {
    stages.push(FeedbackStageEnum.Review)
  }
  return stages
})

const availableStagesReadableGetter = mb.read(function availableStagesReadable (state, getters) {
  let stages = ['initial', 'validate']
  if (Authentication.isReviewer) {
    stages.push('review')
  }
  return stages
})

const availableSubmissionTypeQueryKeysGetter = mb.read(function availableSubmissionTypeQueryKeys (state, getters, rootState) {
  return Object.values(rootState.submissionTypes).map((subType: any) => subType.pk)
})

const availableExamTypeQueryKeysGetter = mb.read(function availableExamTypeQueryKeys (state, getters, rootState) {
  return Object.values(rootState.examTypes).map((examType: any) => examType.pk)
})


function SET_CURRENT_ASSIGNMENT (state: AssignmentsState, assignment?: Assignment): void {
  state.currentAssignment = assignment
}

function SET_CREATE_SUBMISSION_TYPE (state: AssignmentsState, submissionType: string): void {
  state.assignmentCreation.submissionType = submissionType
}

function SET_CREATE_STAGE (state: AssignmentsState, stage: FeedbackStageEnum): void {
  state.assignmentCreation.stage = stage
}

function SET_CREATE_GROUP (state: AssignmentsState, group: Group): void {
  state.assignmentCreation.group = group
}

function SET_SUBMISSION_LEFT (state: AssignmentsState, availableSubmissions: AvailableSubmissionCounts): void {
  state.submissionsLeft = availableSubmissions
}

function SET_GROUPS (state: AssignmentsState, groups: Group[]): void {
  state.groups = groups
}

function UPDATE_CREATE_PARAMETERS_FROM_URL(state: AssignmentsState, route: Route) {
  const submissionType = route.params['sub_type']
  const stage = route.params['stage'] as FeedbackStageEnum
  const group_par = route.params['group']

  state.assignmentCreation.submissionType = submissionType
  state.assignmentCreation.stage = stage
  const group = state.groups.find((group) => group.pk === group_par)
  if (group === undefined && state.groups.length > 0) {
    throw new Error(`Group ${group_par} appeared in parameter but not available in ${state.groups}`)
  }
  state.assignmentCreation.group = group
}

function RESET_STATE (state: AssignmentsState): void {
  Object.assign(state, initialState())
}

async function createNextAssignment({ state }: BareActionContext<AssignmentsState, RootState>) {
  const createAssignment = state.assignmentCreation
  if (createAssignment.submissionType === undefined ) {
    throw new Error('SET_CREATE_SUBMISSION_TYPE needs to be called before createNextAssignment')
  }

  const data = {
    stage: createAssignment.stage,
    submissionType: createAssignment.submissionType!,
    group: createAssignment.group !== undefined ? createAssignment.group.pk : undefined
  }

  Assignments.SET_CURRENT_ASSIGNMENT(await api.createAssignment(data))
}

async function cleanAssignments
({ state }: BareActionContext<AssignmentsState, RootState>) {
  await api.releaseUndoneAssignments()
}

async function changeAssignment
({ state }: BareActionContext<AssignmentsState, RootState>, route: Route) {
  Assignments.UPDATE_CREATE_PARAMETERS_FROM_URL(route)
  if (state.currentAssignment) {
    await Assignments.deleteCurrentAssignment()
  }
  await Assignments.createNextAssignment()
}

async function skipAssignment ({ state }: BareActionContext<AssignmentsState, RootState>) {
  if (!state.currentAssignment) {
    throw new Error('skipAssignment can only be called with active assignment')
  }

  const oldAssignment = state.currentAssignment
  await Assignments.createNextAssignment()
  await api.deleteAssignment({assignment: oldAssignment })

}

async function deleteCurrentAssignment ({ state }: BareActionContext<AssignmentsState
  , RootState>) {
  if (!state.currentAssignment) {
    throw new Error('No active assignment to delete')
  }
  await api.deleteAssignment({assignment: state.currentAssignment})
  Assignments.SET_CURRENT_ASSIGNMENT(undefined)
}

async function getAvailableSubmissionCounts({ state }: BareActionContext<AssignmentsState, RootState>) {
  const counts = await api.fetchAvailableSubmissionCounts(state.assignmentCreation.group)
  Assignments.SET_SUBMISSION_LEFT(counts)
}

async function getGroups() {
  const groups = await api.fetchGroups()
  Assignments.SET_GROUPS(groups)
}


export const Assignments = {
  get state () { return stateGetter() },
  get availableStages () { return availableStagesGetter() },
  get availableStagesReadable () { return availableStagesReadableGetter() },
  get availableSubmissionTypeQueryKeys () { return availableSubmissionTypeQueryKeysGetter() },
  get availableExamTypeQueryKeys () { return availableExamTypeQueryKeysGetter() },

  SET_CURRENT_ASSIGNMENT: mb.commit(SET_CURRENT_ASSIGNMENT),
  SET_CREATE_SUBMISSION_TYPE: mb.commit(SET_CREATE_SUBMISSION_TYPE),
  SET_CREATE_STAGE: mb.commit(SET_CREATE_STAGE),
  SET_CREATE_GROUP: mb.commit(SET_CREATE_GROUP),
  SET_SUBMISSION_LEFT: mb.commit(SET_SUBMISSION_LEFT),
  SET_GROUPS: mb.commit(SET_GROUPS),
  UPDATE_CREATE_PARAMETERS_FROM_URL: mb.commit(UPDATE_CREATE_PARAMETERS_FROM_URL),
  RESET_STATE: mb.commit(RESET_STATE),


  cleanAssignments: mb.dispatch(cleanAssignments),
  changeAssignment: mb.dispatch(changeAssignment),
  createNextAssignment: mb.dispatch(createNextAssignment),
  skipAssignment: mb.dispatch(skipAssignment),
  deleteCurrentAssignment: mb.dispatch(deleteCurrentAssignment),
  getAvailableSubmissionCounts: mb.dispatch(getAvailableSubmissionCounts),
  getGroups: mb.dispatch(getGroups)
}
