import { FeedbackLabel } from '@/models'
import { getStoreBuilder } from 'vuex-typex'
import { RootState } from '../store'
import * as api  from '@/api'
import Vue from 'vue'

export interface FeedbackLabelsState {
  labels: FeedbackLabel[]
}

function initialState(): FeedbackLabelsState {
  return {
    labels: []
  }
}

const mb = getStoreBuilder<RootState>().module('FeedbackLabels', initialState())

const stateGetter = mb.state()

const availableLabelsGetter = mb.read(function labels(state) {
  return state.labels
})

function SET_LABELS(state: FeedbackLabelsState, labels: FeedbackLabel[]) {
  state.labels = labels
}

function ADD_LABEL(state: FeedbackLabelsState, label: FeedbackLabel) {
  state.labels.push(label)
}

function REMOVE_LABEL(state: FeedbackLabelsState, label: FeedbackLabel) {
  state.labels = state.labels.filter((val) => {
    return val.pk !== label.pk
  })
}

function UPDATE_LABEL(state: FeedbackLabelsState, label: FeedbackLabel) {
  REMOVE_LABEL(state, label)
  ADD_LABEL(state, label)
}

async function getLabels() {
  const labels = await api.getLabels()
  FeedbackLabels.SET_LABELS(labels)
}

export const FeedbackLabels = {
  get state() { return stateGetter() },
  get availableLabels() { return availableLabelsGetter() },

  SET_LABELS: mb.commit(SET_LABELS),
  ADD_LABEL: mb.commit(ADD_LABEL),
  REMOVE_LABEL: mb.commit(REMOVE_LABEL),
  UPDATE_LABEL: mb.commit(UPDATE_LABEL),

  getLabels: mb.dispatch(getLabels)
}

