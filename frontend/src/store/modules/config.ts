import { getStoreBuilder } from 'vuex-typex'
import { RootState } from '@/store/store'
import { Config } from '@/models'
import * as api from '@/api'

export interface ConfigState {
  config: Config
}

function initialState (): ConfigState {
  return {
    config: {
      timeDelta: 0,
      currentExam: '',
      examId: '',
      version: '',
      instanceSettings: {
        exerciseMode: false,
        singleCorrection: false,
        stopOnPass: false,
        showSolutionToStudents: false,
      }
    }
  }
}

const mb = getStoreBuilder<RootState>().module('ConfigModule', initialState())

const stateGetter = mb.state()

function SET_CONFIG (state: ConfigState, config: Config) {
  let exam_tmp = state.config.currentExam
  let examId_tmp = state.config.examId
  state.config = config
  state.config.currentExam = exam_tmp
  state.config.examId = examId_tmp
}

function SET_CURRENT_EXAM (state: ConfigState, exam: string) {
  state.config.currentExam = exam
}

function SET_CURRENT_EXAM_ID (state: ConfigState, pk: string) {
  state.config.examId = pk
}

async function getConfig() {
  const config = await api.fetchConfig()
  ConfigModule.SET_CONFIG(config)
}


export const ConfigModule = {
  get state () { return stateGetter() },

  SET_CONFIG: mb.commit(SET_CONFIG),
  SET_CURRENT_EXAM: mb.commit(SET_CURRENT_EXAM),
  SET_CURRENT_EXAM_ID: mb.commit(SET_CURRENT_EXAM_ID),

  getConfig: mb.dispatch(getConfig)
}
