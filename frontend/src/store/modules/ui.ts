import { getStoreBuilder } from 'vuex-typex'
import { RootState } from '@/store/store'

export interface UIState {
  sideBarCollapsed: boolean
  showJumbotron: boolean,
  showSubmissionType: boolean,
}

function initialState (): UIState {
  return {
    sideBarCollapsed: false,
    showJumbotron: true,
    showSubmissionType: true,
  }
}

const mb = getStoreBuilder<RootState>().module('UI', initialState())

const stateGetter = mb.state()

function SET_SIDEBAR_COLLAPSED (state: UIState, collapsed: boolean) {
  state.sideBarCollapsed = collapsed
}
function SET_SHOW_JUMBOTRON (state: UIState, val: boolean) {
  state.showJumbotron = val
}
function SET_SHOW_SUBMISSIONTYPE (state: UIState, val: boolean) {
  state.showSubmissionType = val
}

export const UI = {
  get state () { return stateGetter() },

  SET_SIDEBAR_COLLAPSED: mb.commit(SET_SIDEBAR_COLLAPSED),
  SET_SHOW_JUMBOTRON: mb.commit(SET_SHOW_JUMBOTRON),
  SET_SHOW_SUBMISSIONTYPE: mb.commit(SET_SHOW_SUBMISSIONTYPE),
}
