import { fetchAllFeedback, fetchAllAssignments } from '@/api'
import { objectifyArray } from '@/util/helpers'
import { Assignment, Feedback, FeedbackStageEnum, SubmissionType } from '@/models'
import { RootState } from '@/store/store'
import { getters } from '@/store/getters'
import { getStoreBuilder, BareActionContext } from 'vuex-typex'
import { Authentication } from '@/store/modules/authentication'

export interface FeedbackHistoryItem extends Feedback {
  history?: {
    [key in FeedbackStageEnum]?: {
      ofTutor: string
      isDone: boolean
    }
  }
  mark?: string // not the grade, but the color of the highlighting tool on feedbackHist page
}

export interface FeedbackTableState {
    feedbackHist: {[submissionPk: string]: FeedbackHistoryItem}
}

function initialState (): FeedbackTableState {
  return {
    feedbackHist: {}
  }
}

const mb = getStoreBuilder<RootState>().module('FeedbackTable', initialState())

const stateGetter = mb.state()

function SET_FEEDBACK_HISTORY (state: FeedbackTableState, val: Array<Feedback>) {
  let feedbackList: FeedbackHistoryItem[] = val.map(feedback => {
    return {
      ...feedback,
      mark: 'transparent',
    }
  })
  state.feedbackHist = objectifyArray(feedbackList, 'ofSubmission')
}
function ADD_ASSIGNMENTS_INFO (state: FeedbackTableState, assignments: Array<Assignment>) {
  const doneAssignments = assignments.filter(assignment => assignment.isDone)
  for (const assignment of doneAssignments) {
    if (!assignment.submission || !assignment.stage) {
      throw Error()
    }
    const feedback = state.feedbackHist[assignment.submission as string]
    feedback.history = {
      ...feedback.history,
      [assignment.stage]: {
        ofTutor: assignment.ofTutor,
        isDone: assignment.isDone
      }
    }
  }
}
function SET_FEEDBACK_OF_SUBMISSION_TYPE (state: FeedbackTableState, { feedback, type }:
{feedback: Feedback, type: SubmissionType}) {
  if (!feedback.ofSubmission) {
    throw new Error('Feedback must have ofSubmission present')
  }
  state.feedbackHist[feedback.ofSubmission].ofSubmissionType = type
}
function SET_MARK_COLOR (state: FeedbackTableState, {submissionPk, color}:
{submissionPk: string, color: string}) {
  state.feedbackHist[submissionPk].mark=color
}
function RESET_STATE (state: FeedbackTableState) { Object.assign(state, initialState()) }

function mapFeedbackHistExam ({ state }: BareActionContext<FeedbackTableState, RootState>) {
  for (const feedback of Object.values(state.feedbackHist)) {
    const type = getters.state.examTypes
  }
}

function mapFeedbackHistOfSubmissionType ({ state }: BareActionContext<FeedbackTableState, RootState>) {
  for (const feedback of Object.values(state.feedbackHist)) {
    const type = getters.submissionType((feedback as any).ofSubmissionType)
    FeedbackTable.SET_FEEDBACK_OF_SUBMISSION_TYPE({ feedback, type })
  }
}
async function getFeedbackHistory () {
  let data: [Promise<Feedback[]>, Promise<Assignment[]> | undefined] =
      [fetchAllFeedback(), Authentication.isReviewer ? fetchAllAssignments() : undefined]

  Promise.all<Feedback[], Assignment[] | undefined>(data)
    .then(([feedbacks, assignments]: [Feedback[], Assignment[]?]) => {
      FeedbackTable.SET_FEEDBACK_HISTORY(feedbacks)
      FeedbackTable.mapFeedbackHistOfSubmissionType()
      if (assignments) {
        FeedbackTable.ADD_ASSIGNMENTS_INFO(assignments)
      }
    })
}

export const FeedbackTable = {
  get state () { return stateGetter() },

  SET_FEEDBACK_HISTORY: mb.commit(SET_FEEDBACK_HISTORY),
  ADD_ASSIGNMENTS_INFO: mb.commit(ADD_ASSIGNMENTS_INFO),
  SET_FEEDBACK_OF_SUBMISSION_TYPE: mb.commit(SET_FEEDBACK_OF_SUBMISSION_TYPE),
  SET_MARK_COLOR: mb.commit(SET_MARK_COLOR),
  RESET_STATE: mb.commit(RESET_STATE),

  mapFeedbackHistOfSubmissionType: mb.dispatch(mapFeedbackHistOfSubmissionType),
  getFeedbackHistory: mb.dispatch(getFeedbackHistory)
}
