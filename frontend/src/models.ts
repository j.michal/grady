export interface Config {
  timeDelta: number
  version: string,
  currentExam: string,
  examId: string,
  instanceSettings: {
    [config: string]: boolean,
  }
}

export interface Group {
  pk: string,
  name: string,
  exam: Exam
}

export interface CreateAssignment {
  submissionType: string
  group?: string
  stage: FeedbackStageEnum
}

/**
 *
 * @export
 * @interface Assignment
 */
export interface Assignment {
    /**
     *
     * @type {string}
     * @memberof Assignment
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof Assignment
     */
    submission?: string | SubmissionAssignment
    /**
     *
     * @type {boolean}
     * @memberof Assignment
     */
    isDone?: boolean
    /**
     *
     * @type {string}
     * @memberof Assignment
     */
    owner?: string
    /**
     *
     * @type {string}
     * @memberof Assignment
     */
    stage?: FeedbackStageEnum

    ofTutor?: string

    feedback?: Feedback

}

export interface SubmissionAssignment {
    text: string,
    type: string
    full_score: number,
    tests: Test[]
}

/**
 *
 * @export
 * @interface Exam
 */
export interface Exam {
    /**
     *
     * @type {string}
     * @memberof Exam
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof Exam
     */
    moduleReference: string
    /**
     *
     * @type {number}
     * @memberof Exam
     */
    totalScore: number
    /**
     *
     * @type {number}
     * @memberof Exam
     */
    passScore: number
    /**
     *
     * @type {boolean}
     * @memberof Exam
     */
    passOnly?: boolean
}

/**
 *
 * @export
 * @interface Feedback
 */
export interface Feedback {
    /**
     *
     * @type {number}
     * @memberof Feedback
     */
    pk: number
    /**
     *
     * @type {string}
     * @memberof Feedback
     */
    ofSubmission?: string
    /**
     *
     * @type {boolean}
     * @memberof Feedback
     */
    ofStudent?: string
    isFinal?: boolean
    /**
     *
     * @type {number}
     * @memberof Feedback
     */
    score?: number
    /**
     *
     * @type {Array<FeedbackComment>}
     * @memberof Feedback
     */
    feedbackLines?: {[lineNo: number]: FeedbackComment[]}
    /**
     *
     * @type {Date}
     * @memberof Feedback
     */
    created?: string
    /**
     *
     * @type {Date}
     * @memberof Feedback
     */
    modified?: string
    /**
     *
     * @type {string}
     * @memberof Feedback
     */
    ofSubmissionType: SubmissionType
    /**
     *
     * @type {SubmissionType}
     * @memberof Feedback
     */
    feedbackStageForUser?: string,
    labels: number[],
}

/**
 *
 * @export
 * @interface CreateUpdateFeedback
 */
export interface CreateUpdateFeedback {
    /**
     *
     * @type {number}
     * @memberof Feedback
     */
    pk: number
    /**
     *
     * @type {string}
     * @memberof Feedback
     */
    ofSubmission?: string
    /**
     *
     * @type {boolean}
     * @memberof Feedback
     */
    isFinal?: boolean
    /**
     *
     * @type {number}
     * @memberof Feedback
     */
    score?: number
    /**
     *
     * @type {Array<FeedbackComment>}
     * @memberof Feedback
     */
    feedbackLines: {[lineNo: number]: FeedbackComment}
    /**
     *
     * @type {Date}
     * @memberof Feedback
     */
    created?: string
    /**
     *
     * @type {string}
     * @memberof Feedback
     */
    ofSubmissionType?: string
    /**
     *
     * @type {string}
     * @memberof Feedback
     */
    feedbackStageForUser?: string,
    labels: number[],
}

/**
 *
 * @export
 * @Comment
 */
export interface FeedbackComment {
    /**
     *
     * @type {string}
     * @memberof FeedbackComment
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof FeedbackComment
     */
    text: string
    /**
     *
     * @type {Date}
     * @memberof FeedbackComment
     */
    modified?: string
    /**
     *
     * @type {string}
     * @memberof FeedbackComment
     */
    ofTutor?: string
    /**
     *
     * @type {number}
     * @memberof FeedbackComment
     */
    ofLine?: number
    /**
     *
     * @type {boolean}
     * @memberof FeedbackComment
     */
    visibleToStudent?: boolean
    labels: number[]
    updated?: boolean
}

/**
 *
 * @export
 * @interface FeedbackLabel
 */
export interface FeedbackLabel {
    pk: number
    name: string
    description: string
    colour: string
}

/**
 *
 * @export
 * @interface Credentials
 */
export interface Credentials {
    /**
     *
     * @type {string}
     * @memberof JSONWebToken
     */
    username: string
    /**
     *
     * @type {string}
     * @memberof JSONWebToken
     */
    password: string
}

export interface JSONWebToken {
    token: string
}

export interface Statistics {
    submissionsPerType: number,
    submissionsPerStudent: number,
    currentMeanScore: number,
    submissionTypeProgress: Array<SubmissionTypeProgress>
}

export interface LabelStatisticsForSubType {
    /** Contains the count of the different labels under their pk */
    [label_pk: number]: number,
    /** The pk of the corresponding SubmissionType */
    pk: string
}

/**
 *
 * @export
 * @interface StudentInfo
 */
export interface StudentInfo {
    /**
     *
     * @type {string}
     * @memberof StudentInfo
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof StudentInfo
     */
    name?: string
    /**
     *
     * @type {string}
     * @memberof StudentInfo
     */
    user: string
    /**
     *
     * @type {string}
     * @memberof StudentInfo
     */
    matrikelNo?: string
    /**
     *
     * @type {Exam}
     * @memberof StudentInfo
     */
    exam: Exam
    /**
     *
     * @type {Array<SubmissionList>}
     * @memberof StudentInfo
     */
    submissions: Array<SubmissionList>
    /**
     *
     * @type {boolean}
     * @memberof StudentInfo
     */
    passesExam?: boolean
}

/**
 *
 * @export
 * @interface StudentInfoForListView
 */
export interface StudentInfoForListView {
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    name?: string
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    user?: string
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    userPk?: string
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    exam?: string
    /**
     *
     * @type {Array<SubmissionNoTextFields>}
     * @memberof StudentInfoForListView
     */
    submissions: Array<SubmissionNoTextFields>
    /**
     *
     * @type {string}
     * @memberof StudentInfoForListView
     */
    matrikelNo?: string
    /**
     *
     * @type {boolean}
     * @memberof StudentInfoForListView
     */
    passesExam?: boolean
    /**
     *
     * @type {boolean}
     * @memberof StudentInfoForListView
     */
    isActive: boolean
}

/**
 *
 * @export
 * @interface Submission
 */
export interface Submission {
    /**
     *
     * @type {string}
     * @memberof Submission
     */
    pk: string
    /**
     *
     * @type {SubmissionType}
     * @memberof Submission
     */
    type: SubmissionType
    /**
     *
     * @type {string}
     * @memberof Submission
     */
    text?: string
    /**
     *
     * @type {VisibleCommentFeedback}
     * @memberof Submission
     */
    feedback: VisibleCommentFeedback
    /**
     *
     * @type {Array<Test>}
     * @memberof Submission
     */
    tests: Array<Test>,

    sourceCodeAvailable: boolean
}

/**
 *
 * @export
 * @interface SubmissionList
 */
export interface SubmissionList {
    /**
     *
     * @type {string}
     * @memberof SubmissionList
     */
    pk: string
    /**
     *
     * @type {SubmissionTypeList}
     * @memberof SubmissionList
     */
    type: SubmissionTypeList
    /**
     *
     * @type {Feedback}
     * @memberof SubmissionList
     */
    feedback: Feedback
}

/**
 *
 * @export
 * @interface SubmissionNoTextFields
 */
export interface SubmissionNoTextFields {
    /**
     *
     * @type {string}
     * @memberof SubmissionNoTextFields
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoTextFields
     */
    type: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoTextFields
     */
    score?: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoTextFields
     */
    final?: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoTextFields
     */
    fullScore?: string
}

/**
 *
 * @export
 * @interface SubmissionNoType
 */
export interface SubmissionNoType {
    /**
     *
     * @type {string}
     * @memberof SubmissionNoType
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoType
     */
    type: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoType
     */
    fullScore?: string
    /**
     *
     * @type {string}
     * @memberof SubmissionNoType
     */
    text?: string
    /**
     *
     * @type {Feedback}
     * @memberof SubmissionNoType
     */
    feedback?: Feedback
    ofStudent?:string
    /**
     *
     * @type {Array<Test>}
     * @memberof SubmissionNoType
     */
    tests: Array<Test>,

    sourceCodeAvailable: boolean
}

/**
 *
 * @export
 * @interface SubmissionType
 */
export interface SubmissionType {
    /**
     *
     * @type {string}
     * @memberof SubmissionType
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof SubmissionType
     */
    name: string
    /**
     *
     * @type {number}
     * @memberof SubmissionType
     */
    examType: Exam
    /**
     *
     * @type {Exam}
     * @memberof SubmissionType
     */
    fullScore?: number
    /**
     *
     * @type {string}
     * @memberof SubmissionType
     */
    description: string
    /**
     *
     * @type {string}
     * @memberof SubmissionType
     */
    solution?: string
    /**
     *
     * @type {string}
     * @memberof SubmissionType
     */
    programmingLanguage?: SubmissionType.ProgrammingLanguageEnum

    solutionComments: {[ofLine: number]: SolutionComment[]}
}

export interface AvailableSubmissionCounts {
  [submissionType: string]: {
    [stage: string]: number
  }
}

export interface SolutionComment {
    pk: number,
    created: string,
    ofLine: number,
    ofSubmissionType: string,
    ofUser: string,
    text: string
}

/**
 * @export
 * @namespace SubmissionType
 */
export namespace SubmissionType {
    /**
     * @export
     * @enum {string}
     */
    export enum ProgrammingLanguageEnum {
        C = 'c',
        Java = 'java',
        Markdown = 'markdown'
    }
}

/**
 *
 * @export
 * @interface SubmissionTypeList
 */
export interface SubmissionTypeList {
    /**
     *
     * @type {string}
     * @memberof SubmissionTypeList
     */
    pk: string

    /**
     *
     * @type {string}
     * @memberof SubmissionTypeList
     */
    name: string
    /**
     *
     * @type {number}
     * @memberof SubmissionTypeList
     */
    fullScore?: number
}

export interface SubmissionTypeProgress {
    pk: string
    name: string
    feedbackFinal: number
    feedbackInValidation: number
    feedbackInConflict: number
    submissionCount: number
}


/**
 * @export
 * @enum {string}
 */
export enum FeedbackStageEnum {
    Creation = 'feedback-creation',
    Validation = 'feedback-validation',
    Review = 'feedback-review'
}

/**
 *
 * @export
 * @interface Test
 */
export interface Test {
    /**
     *
     * @type {string}
     * @memberof Test
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof Test
     */
    name: string
    /**
     *
     * @type {string}
     * @memberof Test
     */
    label: string
    /**
     *
     * @type {string}
     * @memberof Test
     */
    annotation: string
}

/**
 *
 * @export
 * @interface Tutor
 */
export interface Tutor {
    /**
     *
     * @type {string}
     * @memberof Tutor
     */
    pk: string
    /**
     *
     * @type {string}
     * @memberof Tutor
     */
    password?: string
    /**
     * Designates whether this user should be treated as active. Unselect this instead of deleting accounts.
     * @type {boolean}
     * @memberof Tutor
     */
    isActive?: boolean
    /**
     * Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
     * @type {string}
     * @memberof Tutor
     */
    username: string
    /**
     *
     * @type {string}
     * @memberof Tutor
     */
    feedbackCreated?: string
    /**
     *
     * @type {string}
     * @memberof Tutor
     */
    feedbackValidated?: string
    /**
     *
     * @type {Group}
     * @memberof Tutor
     */
    exerciseGroups: Group[]

    /**
     * @type {string}
     * @memberof Tutor
     */
    role: UserAccount.RoleEnum
}

/**
 *
 * @export
 * @interface UserAccount
 */
export interface UserAccount {
    /**
     *
     * @type {string}
     * @memberof UserAccount
     */
    pk: string
    /**
     * Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
     * @type {string}
     * @memberof UserAccount
     */
    username?: string
    /**
     *
     * @type {string}
     * @memberof UserAccount
     */
    role?: UserAccount.RoleEnum
    /**
     *
     * @type {boolean}
     * @memberof UserAccount
     */
    isAdmin?: boolean
    /**
     *
     * @type {string}
     * @memberof UserAccount
     */
    password?: string
    exerciseGroups: Group[]
}

/**
 * @export
 * @namespace UserAccount
 */
export namespace UserAccount {
    /**
     * @export
     * @enum {string}
     */
    export enum RoleEnum {
        Student = 'Student',
        Tutor = 'Tutor',
        Reviewer = 'Reviewer'
    }
}

/**
 *
 * @export
 * @interface VisibleCommentFeedback
 */
export interface VisibleCommentFeedback {
    /**
     *
     * @type {number}
     * @memberof VisibleCommentFeedback
     */
    pk: number
    /**
     *
     * @type {string}
     * @memberof VisibleCommentFeedback
     */
    ofSubmission: string
    /**
     *
     * @type {boolean}
     * @memberof VisibleCommentFeedback
     */
    isFinal?: boolean
    /**
     *
     * @type {number}
     * @memberof VisibleCommentFeedback
     */
    score?: number
    /**
     *
     * @type {string}
     * @memberof VisibleCommentFeedback
     */
    feedbackLines?: string
    /**
     *
     * @type {Date}
     * @memberof VisibleCommentFeedback
     */
    created?: Date
    /**
     *
     * @type {string}
     * @memberof VisibleCommentFeedback
     */
    ofSubmissionType?: string

    labels: number[]
}

export interface GitlabRelease {

    tag_name: string

    description_html: string

}
