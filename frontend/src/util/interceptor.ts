import instance from '@/main'
import { parseErrorNotification, parseBlacklist } from '@/util/helpers'


const errorUrlBlacklist = [
  '/api/get-token/',
  '/api/.*/change_password/',
  '/api/corrector/register/',
  '/api/assignment/'
]
const blackListRegExp = new RegExp(parseBlacklist(errorUrlBlacklist), 'g')

export function errorInterceptor (error: any): any {
  if (!error.response.request.responseURL.match(blackListRegExp)) {
    instance.$notify({
      title: 'Request failed.',
      text: parseErrorNotification(error.response),
      type: 'error',
      duration: -1
    })
  }
  return Promise.reject(error)
}
