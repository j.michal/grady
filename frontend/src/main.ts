
import './class-component-hooks'

import Vue from 'vue'
import store from './store/store'
import App from './App.vue'
import router from './router/index'
import Vuetify from 'vuetify'
import Notifications from 'vue-notification'
import Clipboard from 'v-clipboard'

import 'vuetify/dist/vuetify.min.css'
import 'highlight.js/styles/atom-one-light.css'

import '@/util/shortkeys'

Vue.use(Vuetify)

Vue.use(Clipboard)
Vue.use(Notifications)

Vue.config.productionTip = false

const el = process.env.NODE_ENV === 'test' ? undefined : '#app'

const vuetify = new Vuetify({
 icons: {
    iconfont: 'md',
  },
})

export default new Vue({
  vuetify,
  el: el,
  router: router,
  store,
  render: h => h(App)
}).$mount(el)
