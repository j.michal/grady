import Vue from 'vue'
import Component, { mixins } from 'vue-class-component'
import { fetchStudentExportData, StudentExportItem, InstanceExportData, fetchInstanceExportData } from '@/api'
import { getters } from '@/store/getters'
import { mutations as mut } from '@/store/mutations'
import { saveAs } from 'file-saver'
import { Exam } from '@/models'
import { ConfigModule } from '../../store/modules/config'

let download = saveAs

export enum ExportType {
  JSON = 'application/json',
}

export function mock(mockedDownload?: () => boolean) {
  download = mockedDownload || saveAs
}

@Component
export class exportMixin extends Vue {
  exportDialog = true
  mapFile: File | null = null
  setPasswords = false
  exportType = ExportType.JSON
  loading = false

  get mapFileLoaded () {
    return Object.keys(getters.state.studentMap).length > 0
  }

  async getExportFile (type: string) {
    this.loading = true

    let selected_exam = ConfigModule.state.config.examId

    let studentData
    if (type === 'data') {
      studentData = await fetchStudentExportData({ setPasswords: this.setPasswords, selectedExam: selected_exam })
    } else if (type === 'instance') {
      studentData = await fetchInstanceExportData()
    } else {
      throw new Error('Unsupported export type')
    }

    if (this.mapFile || this.mapFileLoaded) {
      this.getMappedExportFile(studentData)
    } else {
      this.optionalConvertAndCreatePopup(studentData)
    }
  }

  optionalConvertAndCreatePopup (studentData: StudentExportItem[] | InstanceExportData) {
    const convertedData = JSON.stringify(studentData)
    const filename = 'export.json'
    download(new Blob([convertedData], { type: this.exportType }), filename)
    this.loading = false
  }

  async getMappedExportFile (studentData: StudentExportItem[] | InstanceExportData) {
    if (!this.mapFile && !this.mapFileLoaded) {
      throw new Error('Either mapFile must be selected or already loaded ' +
                      'to call getMappedExportFile')
    }
    if (this.mapFile) {
      await this.readMapFileAndCommit()
    }
    this.applyMapping(studentData)
    this.optionalConvertAndCreatePopup(studentData)
  }

  readMapFileAndCommit (): Promise<void> {
    const fileReader = new FileReader()
    return new Promise((resolve, reject) => {
      fileReader.onload = event => {
        // @ts-ignore typings of EventTarget seem to be wrong
        const studentMap = JSON.parse(event.target.result)
        mut.SET_STUDENT_MAP(studentMap)
        resolve()
      }
      fileReader.onerror = () => {
        fileReader.abort()
        reject(new Error('Problem parsing input file.'))
      }

      if (!this.mapFile) {
        reject(new Error('Can only call' +
          ' readMapFileAndCommit when mapFile is not undefined'))
      } else {
        fileReader.readAsText(this.mapFile)
      }
    })
  }

  hide () {
    this.$emit('hide')
  }

  showDialog () {
    this.exportDialog = true
  }

  applyMapping (exportData: StudentExportItem[] | InstanceExportData): void { throw new Error('Not implemented.') }
}
