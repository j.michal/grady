from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from util.factories import make_test_data, make_exams
from functional_tests.util import GradyTestCase, reset_browser_after_test


class TestMultipleExams(GradyTestCase):

    def setUp(self):
        exams = make_exams([{
            'module_reference': 'Test Exam 01',
            'total_score': 100,
            'pass_score': 60,
        }, {
            'module_reference': 'Test Exam 02',
            'total_score': 120,
            'pass_score': 75}]
        )

        self.test_data = make_test_data(data_dict={
            'exams': [{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
                'exam_type_id': exams[0].exam_type_id
            }, {
                'module_reference': 'Test Exam 02',
                'total_score': 120,
                'pass_score': 75,
                'exam_type_id': exams[1].exam_type_id
            }],
            'submission_types': [
                {
                    'name': '01. Sort this or that',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!',
                    'exam_type': exams[0]
                },
                {
                    'name': '02. Merge this or that or maybe even this',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!',
                    'exam_type': exams[0]
                },
                {
                    'name': '05',
                    'full_score': 35,
                    'description': 'It is not a story the jedi would tell you',
                    'solution': 'Trivial!',
                    'exam_type': exams[1]
                }
            ],
            'students': [
                {
                    'username': 'student01',
                    'password': 'p',
                    'exam': 'Test Exam 01'
                },
                {
                    'username': 'student02',
                    'password': 'p',
                    'exam': 'Test Exam 01'
                },
                {
                    'username': 'student03',
                    'password': 'p',
                    'exam': 'Test Exam 02'

                },
                {
                    'username': 'student04',
                    'password': 'p',
                    'exam': 'Test Exam 02'
                }
            ],
            'tutors': [
                {'username': 'tutor01', 'password': 'p'},
                {'username': 'tutor02', 'password': 'p'}
            ],
            'reviewers': [
                {'username': 'reviewer', 'password': 'p'}
            ],
            'submissions': [
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student01',
                    'feedback': {
                        'score': 5,
                        'is_final': True,
                        'feedback_lines': {
                            '1': [{
                                'text': 'This is very bad!',
                                'of_tutor': 'tutor01'
                            }],
                        }

                    }
                },
                {
                    'text': 'function blabl\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student01'
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student02'
                },
                {
                    'text': 'function lorem ipsum etc\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student02'
                }
            ]}
        )

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self, account):
        self.browser.get(self.live_server_url)
        username_input = self.browser.find_element_by_xpath('//input[@id="username"]')
        username_input.send_keys(account.username)
        password_input = self.browser.find_element_by_xpath('//input[@id="password"]')
        password_input.send_keys('p')
        self.browser.find_element_by_xpath('//button[@type="submit"]').send_keys(Keys.ENTER)
        WebDriverWait(self.browser, 10).until(ec.url_contains('/exam_selection'))

    def test_selection_page(self):
        reviewer = self.test_data['reviewers'][0]
        self._login(reviewer)
        self.assertTrue(self.browser.current_url.endswith('#/exam_selection'))

    def test_selection_of_exams(self):
        testBool = True
        reviewer = self.test_data['reviewers'][0]
        self._login(reviewer)
        items = self.browser.find_elements_by_id("listItem")
        for i in range(len(items)):
            itemText = items[i].text
            items[i].click()
            testBool = itemText == self.browser.find_element_by_class_name("title").text
            if(not testBool):
                break
            self.browser.find_element_by_id("examsButton").click()
            items = self.browser.find_elements_by_id("listItem")
        self.assertTrue(testBool)
