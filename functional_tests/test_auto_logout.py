import datetime
import logging
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

from core.models import UserAccount
from functional_tests.util import (GradyTestCase, login, reset_browser_after_test)
from util import factory_boys as fact

from rest_framework_jwt.settings import api_settings

log = logging.getLogger(__name__)


class TestAutoLogout(GradyTestCase):
    username = None
    password = None
    role = None

    def setUp(self):
        self.username = 'reviewer'
        self.password = 'p'
        self.role = UserAccount.TUTOR
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
            role=self.role
        )

    def tearDown(self):
        api_settings.reload()
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def test_auto_logout_can_continue(self):
        with self.settings(JWT_AUTH={
            'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=15),
            'JWT_ALLOW_REFRESH': True,
        }
        ):
            self._login()
            initial_token = self.browser.execute_script(
                'return document.getElementById("app").__vue__.$store'
                '._modules.root.state.Authentication.token'
            )
            logout_dialog = self.browser.find_element_by_id('logout-dialog')
            WebDriverWait(self.browser, 15, 0.2).until(
                ec.visibility_of_element_located((By.ID, 'logout-dialog')))
            # the below line should work for clicking the button, but something
            # obscures it, thus the workaround below the comment
            # logout_dialog.find_element_by_id('continue-btn').click()
            continue_btn = logout_dialog.find_element_by_id('continue-btn')
            ActionChains(self.browser).move_to_element(continue_btn).click().perform()
            WebDriverWait(self.browser, 15, 0.2).until(
                ec.invisibility_of_element_located((By.ID, 'logout-dialog')))
            self.assertNotIn('login', self.browser.current_url)
            new_token = self.browser.execute_script(
                'return document.getElementById("app").__vue__.$store.'
                '_modules.root.state.Authentication.token'
            )
            self.assertNotEqual(initial_token, new_token)
