from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from core.models import FeedbackLabel
from functional_tests.util import (GradyTestCase, login, reset_browser_after_test,
                                   query_returns_object, go_to_subscription,
                                   reconstruct_submission_code, wait_until_code_changes)
from util import factory_boys as fact


class FeedbackLabelSystemTest(GradyTestCase):
    username = None
    password = None
    role = None

    def setUp(self):
        super().setUp()
        self.username = 'tut'
        self.password = 'p'
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
        )
        self.sub_type = fact.SubmissionTypeFactory.create()
        fact.SubmissionFactory.create_batch(2, type=self.sub_type)

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    # creates a new label where colour_num is
    # the index of the colour to click on the colour picker
    def create_label(self, name, description, colour_num):
        self.browser.find_element_by_id('create-label-section').click()
        WebDriverWait(self.browser, 2).until(
            ec.element_to_be_clickable((By.ID, 'label-name'))
        )
        self.browser.find_element_by_id('label-name').send_keys(name)
        self.browser.find_element_by_id('label-description').send_keys(description)
        self.browser.find_elements_by_class_name('v-color-picker__color')[colour_num].click()
        self.browser.find_element_by_id('create-label-btn').click()
        WebDriverWait(self.browser, 10).until(query_returns_object(FeedbackLabel, name=name))
        self.browser.find_element_by_class_name('notification-title').click()

    # updates an already existing label with the given arguments
    def update_label(self, old_name, new_name, description, colour_num):
        self.browser.find_element_by_id('update-label-section').click()
        WebDriverWait(self.browser, 2).until(
            ec.element_to_be_clickable((By.ID, 'label-update-autocomplete'))
        )
        old_name_input = self.browser.find_element_by_id('label-update-autocomplete')
        old_name_input.click()
        old_name_input.send_keys(old_name)
        self.browser.find_element_by_class_name('label-updater-item').click()
        self.browser.find_element_by_xpath(
            '//div[contains(@class, "v-window-item--active")]//input[@id="label-name"]'
        ).send_keys(new_name)

        self.browser.find_element_by_xpath(
            '//div[contains(@class, "v-window-item--active")]//textarea[@id="label-description"]'
        ).send_keys(description)

        self.browser.find_elements_by_xpath(
            '//div[contains(@class, "v-window-item--active")]'
            '//div[contains(@class, "v-color-picker__color")]'
        )[colour_num].click()
        self.browser.find_element_by_id('update-label-btn').click()
        WebDriverWait(self.browser, 10).until(
            query_returns_object(FeedbackLabel, name=old_name + new_name)
        )
        self.browser.find_element_by_class_name('notification-title').click()

    def assign_label_to_feedback(self, name):
        label_input = self.browser.find_element_by_xpath(
            '//div[@id="feedback-label-selector"]//input[@id="label-add-autocomplete"]'
        )
        label_input.click()
        label_input.send_keys(name)
        self.browser.find_element_by_class_name('label-adder-item').click()

    def remove_label_from_feedback(self, name):
        self.browser.find_element_by_xpath(
            f'//div[@id="feedback-label-selector"]//span[contains(@class, "v-chip__content") '
            f'and contains(text(), "{name}")]//button[contains(@class, "v-chip__close")]'
        ).click()

    def assign_label_to_comment_line(self, line, name):
        self.browser.find_element_by_xpath(
            f'//span[contains(@class, "v-btn__content") and contains(text(), "{line}")]'
        ).click()
        label_input = self.browser.find_element_by_xpath(
            '//div[@id="comment-label-selector"]//input[@id="label-add-autocomplete"]'
        )
        label_input.click()
        label_input.send_keys(name)
        self.browser.find_element_by_class_name('label-adder-item').click()
        self.browser.find_element_by_id('submit-comment').click()

    def remove_label_from_comment_line(self, line, name):
        self.browser.find_element_by_xpath(
            f'//tr[@id="sub-line-{line}"]//span[contains(text(), "{name}")]'
            '//button[contains(@class, "v-chip__close")]'
        ).click()

    # Removes any notification that could obstruct buttons.
    def check_for_notification(self):
        try:
            self.browser.find_element_by_class_name('notification').click()
        except NoSuchElementException:
            pass

    def test_can_create_label(self):
        self._login()
        label_name = 'test name'
        label_desc = 'test description'
        self.create_label(label_name, label_desc, 3)
        created_label = FeedbackLabel.objects.get(name='test name')

        self.assertEqual(created_label.name, label_name)
        self.assertEqual(created_label.description, label_desc)

    def test_can_not_create_duplicate_label(self):
        self._login()
        label_name = 'duplicate'
        label_desc = 'duplicate test'
        self.create_label(label_name, label_desc, 3)
        self.create_label(label_name, label_desc, 3)
        WebDriverWait(self.browser, 2).until(
            ec.visibility_of_element_located((By.CLASS_NAME, 'notification-content'))
        )
        notification = self.browser.find_element_by_class_name('notification-content')

        labels = FeedbackLabel.objects.all()
        self.assertIn('already exists', notification.text)
        self.assertEqual(len(labels), 1)

    def test_can_update_label(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        self.update_label('test', 'updated', 'updated desc', 3)

        label = FeedbackLabel.objects.get(name='testupdated')

        self.assertEqual(label.name, 'testupdated')
        self.assertEqual(label.description, 'some descupdated desc')

    def test_can_assign_label_to_feedback_draft(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        self.assign_label_to_feedback('test')
        labels = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        self.assertGreater(len(labels), 1)

        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        label = FeedbackLabel.objects.get(name='test')
        self.assertEqual(len(label.feedback.all()), 1)

    def test_can_remove_label_from_feedback_draft(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        go_to_subscription(self)
        self.assign_label_to_feedback('test')
        self.remove_label_from_feedback('test')
        labels = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )

        self.assertEqual(len(labels), 1)

    def test_can_remove_label_from_submitted_feedback(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        self.assign_label_to_feedback('test')
        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        # logs out user
        reset_browser_after_test(self.browser, self.live_server_url)

        username = 'tut_snd'
        password = 'p'
        fact.UserAccountFactory(username=username, password=password)
        login(self.browser, self.live_server_url, username, password)

        go_to_subscription(self, stage='validate')
        self.remove_label_from_feedback('test')
        added = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        removed = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE REMOVED")]/..//*'
        )
        current = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "CURRENT LABELS")]/..//*'
        )

        self.assertGreater(len(removed), 1)
        self.assertEqual(len(current), 1)
        self.assertEqual(len(added), 1)

        self.browser.find_element_by_id('submit-feedback').click()
        sub_url = 'correction/ended'
        WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))
        label = FeedbackLabel.objects.get(name='test')

        self.assertEqual(len(label.feedback.all()), 0)

    def test_can_add_label_to_submitted_feedback(self):
        self._login()
        self.create_label('test', 'some test dec', 1)
        self.create_label('add', 'add test dec', 4)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        self.assign_label_to_feedback('test')
        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        # logs out user
        reset_browser_after_test(self.browser, self.live_server_url)

        username = 'tut_snd'
        password = 'p'
        fact.UserAccountFactory(username=username, password=password)
        login(self.browser, self.live_server_url, username, password)
        go_to_subscription(self, stage='validate')

        self.assign_label_to_feedback('add')
        added = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        removed = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "WILL BE REMOVED")]/..//*'
        )
        current = self.browser.find_elements_by_xpath(
            '//div[@id="feedback-label-selector"]//div[contains(text(), "CURRENT LABELS")]/..//*'
        )

        self.assertEqual(len(removed), 1)
        self.assertGreater(len(added), 1)
        self.assertGreater(len(current), 1)

        self.browser.find_element_by_id('submit-feedback').click()
        sub_url = 'correction/ended'
        WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))
        new_label = FeedbackLabel.objects.get(name='add')
        old_label = FeedbackLabel.objects.get(name='test')

        self.assertEqual(len(old_label.feedback.all()), 1)
        self.assertEqual(len(new_label.feedback.all()), 1)

    def test_can_assign_label_to_comment(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        comment_line = 1
        self.assign_label_to_comment_line(comment_line, 'test')
        added = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        self.assertGreater(len(added), 1)

        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        label = FeedbackLabel.objects.get(name='test')
        self.assertEqual(len(label.feedback_comments.all()), 1)

    def test_can_remove_label_from_submitted_comment(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        comment_line = 1
        self.assign_label_to_comment_line(comment_line, 'test')
        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        # logs out user
        reset_browser_after_test(self.browser, self.live_server_url)

        username = 'tut_snd'
        password = 'p'
        fact.UserAccountFactory(username=username, password=password)
        login(self.browser, self.live_server_url, username, password)

        go_to_subscription(self, stage='validate')
        self.browser.find_element_by_id('feedback-visibility-toggle').click()

        self.remove_label_from_comment_line(comment_line, 'test')
        added = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        removed = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "WILL BE REMOVED")]/..//*'
        )
        current = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "CURRENT LABELS")]/..//*'
        )

        self.assertGreater(len(removed), 1)
        self.assertEqual(len(added), 1)
        self.assertEqual(len(current), 1)

        self.browser.find_element_by_id('submit-feedback').click()
        sub_url = 'correction/ended'
        WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))
        label = FeedbackLabel.objects.get(name='test')

        # comment still exists but is now invisible
        self.assertEqual(label.feedback_comments.all()[0].visible_to_student, False)

    def test_can_add_label_to_submitted_comment(self):
        self._login()
        self.create_label('test', 'some desc', 1)
        self.create_label('add', 'add test desc', 4)
        go_to_subscription(self)
        code = reconstruct_submission_code(self)
        comment_line = 1
        self.assign_label_to_comment_line(comment_line, 'test')
        self.browser.find_element_by_id('score-full').click()
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            wait_until_code_changes(self, code)
        )

        # logs out user
        reset_browser_after_test(self.browser, self.live_server_url)

        username = 'tut_snd'
        password = 'p'
        fact.UserAccountFactory(username=username, password=password)
        login(self.browser, self.live_server_url, username, password)

        go_to_subscription(self, stage='validate')
        self.browser.find_element_by_id('feedback-visibility-toggle').click()
        self.assign_label_to_comment_line(comment_line, 'add')
        added = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "WILL BE ADDED")]/..//*'
        )
        removed = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "WILL BE REMOVED")]/..//*'
        )
        current = self.browser.find_elements_by_xpath(
            f'//tr[@id="sub-line-{comment_line}"]//div[contains(text(), "CURRENT LABELS")]/..//*'
        )

        self.assertEqual(len(removed), 1)
        self.assertGreater(len(current), 1)
        self.assertGreater(len(added), 1)

        self.browser.find_element_by_id('submit-feedback').click()
        sub_url = 'correction/ended'
        WebDriverWait(self.browser, 10).until(ec.url_contains(sub_url))
        label = FeedbackLabel.objects.get(name='add')

        # comment still exists but is now invisible
        self.assertEqual(len(label.feedback_comments.all()), 1)
