import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


from core import models
from functional_tests.util import (GradyTestCase, login, query_returns_object,
                                   reset_browser_after_test)
from util import factory_boys as fact


JSON_EXPORT_FILE = os.path.join(os.path.dirname(__file__), 'data/hektor.json')


class TestImport(GradyTestCase):
    username = None
    password = None
    role = None

    def setUp(self):
        super().setUp()
        self.username = 'rev'
        self.password = 'p'
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
            role=models.UserAccount.REVIEWER
        )

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def test_reviewer_can_import_data(self):
        self._login()
        self.browser.find_element_by_id("import-data-list-item").click()
        file_input = self.browser.find_element_by_id("file-input")
        file_input.send_keys(JSON_EXPORT_FILE)
        # self.browser.find_element_by_id("submit-import").click()
        import_btn = self.browser.find_element_by_id('submit-import')
        ActionChains(self.browser).move_to_element(import_btn).click().perform()
        WebDriverWait(self.browser, 20).until(query_returns_object(models.SubmissionType))
