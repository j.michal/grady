from selenium.webdriver.support.ui import WebDriverWait

from core import models
from core.models import UserAccount
from functional_tests.util import (GradyTestCase, login, subscriptions_loaded_cond,
                                   extract_hrefs_hashes, reset_browser_after_test)
from util import factory_boys as fact


# This is a little hack to have Super test class which implements common behaviour
# and tests but is not executed. In order to have the testrunner ignore the
# FrontPageTestsTutorReviewer class we need to define it within a class which does not inherit from
# unittest
class UntestedParent:
    class FrontPageTestsTutorReviewer(GradyTestCase):
        username = None
        password = None
        role = None

        def setUp(self):
            fact.SubmissionFactory.create_batch(4)

        def tearDown(self):
            self.saveScreenshots()
            reset_browser_after_test(self.browser, self.live_server_url)

        def _login(self):
            login(self.browser, self.live_server_url, self.username, self.password)

        def test_statistics_are_shown(self):
            self._login()
            statistics = self.browser.find_element_by_id('correction-statistics')
            title = statistics.find_element_by_class_name('title')
            self.assertEqual('Statistics', title.text)

        def test_available_tasks_are_shown(self):
            self._login()
            WebDriverWait(self.browser, 10).until(subscriptions_loaded_cond(self.browser))
            tasks = self.browser.find_element_by_name('subscription-list')
            submission_type_links = extract_hrefs_hashes(
                tasks.find_elements_by_tag_name('a')
            )
            sub_types = models.SubmissionType.objects.all()
            default_group = models.Group.objects.first()
            for sub_type in sub_types:
                self.assertIn(f'/correction/{sub_type.pk}/feedback-creation/{default_group.pk}',
                              submission_type_links)


class FrontPageTestsTutor(UntestedParent.FrontPageTestsTutorReviewer):
    def setUp(self):
        super().setUp()
        self.username = 'tutor'
        self.password = 'p'
        self.role = UserAccount.TUTOR
        fact.UserAccountFactory(
            username=self.username,
            password=self.password
        )

    def tearDown(self):
        reset_browser_after_test(self.browser, self.live_server_url)

    def test_side_bar_contains_correct_items(self):
        self._login()
        drawer = self.browser.find_element_by_class_name('v-navigation-drawer')
        links = extract_hrefs_hashes(drawer.find_elements_by_tag_name('a'))
        print(links)
        self.assertTrue(all(link in links for link in ['/home', '/feedback']))
        footer = drawer.find_element_by_class_name('sidebar-footer')
        feedback_link = footer.find_element_by_css_selector('#feedback-btn')
        self.assertEqual('https://gitlab.gwdg.de/j.michal/grady/issues',
                         feedback_link.get_attribute('href'))


class FrontPageTestsReviewer(UntestedParent.FrontPageTestsTutorReviewer):
    def setUp(self):
        super().setUp()
        self.username = 'reviewer'
        self.password = 'p'
        self.role = UserAccount.REVIEWER
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
            role=self.role
        )

    def tearDown(self):
        reset_browser_after_test(self.browser, self.live_server_url)

    def test_side_bar_contains_correct_items(self):
        self._login()
        drawer = self.browser.find_element_by_class_name('v-navigation-drawer')
        links = extract_hrefs_hashes(drawer.find_elements_by_tag_name('a'))
        self.assertTrue(all(link in links for link in
                            ['/home', '/feedback', '/participant-overview', '/tutor-overview']))
        footer = drawer.find_element_by_class_name('sidebar-footer')
        feedback_link = footer.find_element_by_css_selector('#feedback-btn')
        self.assertEqual('https://gitlab.gwdg.de/j.michal/grady/issues',
                         feedback_link.get_attribute('href'))
