from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait

from core.models import UserAccount
from functional_tests.util import (GradyTestCase, login, go_to_subscription,
                                   reconstruct_submission_code, correct_some_submission,
                                   reset_browser_after_test)
from util import factory_boys as fact


class TestFeedbackUpdate(GradyTestCase):
    username = None
    password = None

    def setUp(self):
        super().setUp()
        self.username = 'tut'
        self.password = 'p'
        fact.UserAccountFactory(
            username=self.username,
            password=self.password,
            role=UserAccount.TUTOR
        )
        self.sub_type = fact.SubmissionTypeFactory.create()
        fact.SubmissionFactory.create_batch(2, type=self.sub_type)

    def tearDown(self):
        self.saveScreenshots()
        reset_browser_after_test(self.browser, self.live_server_url)

    def _login(self):
        login(self.browser, self.live_server_url, self.username, self.password)

    def test_updating_own_feedback_doesnt_invalidate_other_tutors_assignment(self):
        # First correct some submission as the first tutor
        self._login()
        code = correct_some_submission(self)
        first_tab = self.browser.current_window_handle

        # open a new tab and go to the validation page of the just corrected submission
        self.browser.execute_script('window.open()')
        self.browser.switch_to.window(self.browser.window_handles[-1])
        self.browser.get(self.live_server_url)
        second_tab = self.browser.current_window_handle
        username = 'other_tut'
        password = 'p'
        fact.UserAccountFactory(
            username=username,
            password=password,
            role=UserAccount.TUTOR
        )
        login(self.browser, self.live_server_url, username, password)
        go_to_subscription(self, stage='validate')
        other_code = reconstruct_submission_code(self)

        # The submission to be validated should be the same as the one that has been corrected
        self.assertEqual(
            code, other_code,
            "Code for validation submissions is different than initial")

        # Go to first tab and update the submission as the first tutor via the Feedback History page
        self.browser.switch_to.window(first_tab)
        self.browser.find_element_by_partial_link_text('Feedback History').click()
        WebDriverWait(self.browser, 15, 0.2).until(
            ec.visibility_of_element_located((By.CLASS_NAME, 'feedback-row')))
        feedback_entry = self.browser.find_element_by_class_name('feedback-row')
        ActionChains(self.browser).move_to_element(feedback_entry).click().perform()

        self.browser.find_element_by_id('submit-feedback').click()

        # as the second tutor, submit the validated feedback
        self.browser.switch_to.window(second_tab)
        self.browser.find_element_by_id('submit-feedback').click()
        WebDriverWait(self.browser, 10).until(
            ec.url_contains('ended'),
            'Browser is not on Subscription ended site, therefore Feedback could not be submitted'
        )
