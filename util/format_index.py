import fileinput
import sys
import re

file = 'core/templates/index.html'

STATIC_FILES_REGEX = re.compile("=/static/(.*?)([ >])")
SUB_PATTERN = r"={% static '\1' %}\2"

with open(file, "r+") as f:
    s = f.read()
    f.seek(0)
    f.write("{% load static %}\n" + s)

for line in fileinput.input(file, inplace=1):
    sys.stdout.write(STATIC_FILES_REGEX.sub(SUB_PATTERN, line))
