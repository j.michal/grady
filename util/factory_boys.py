import factory
from factory.django import DjangoModelFactory
from faker import Faker
from core import models

Faker.seed(42)
fake = Faker()


class ExamTypeFactory(DjangoModelFactory):
    class Meta:
        model = models.ExamType
        django_get_or_create = ('module_reference',)

    module_reference = 'B.Inf.4242 Test Module'
    total_score = 90
    pass_score = 45
    pass_only = False


class SubmissionTypeFactory(DjangoModelFactory):
    class Meta:
        model = models.SubmissionType
    name = factory.Sequence(lambda n: f"[{n}] Example submission type")
    full_score = 15
    description = factory.Sequence(
        lambda n: f'Type {n} \n<h1>This</h1> is a description containing html')
    solution = factory.Sequence(lambda n: f'test case {n}')  # noqa
    programming_language = models.SubmissionType.C
    exam_type = factory.SubFactory(ExamTypeFactory)


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = models.Group
    name = factory.Sequence(lambda n: f"Group [{n}]")
    exam = factory.SubFactory(ExamTypeFactory)


class UserAccountFactory(DjangoModelFactory):
    class Meta:
        model = models.UserAccount
        django_get_or_create = ('username',)

    role = models.UserAccount.TUTOR
    fullname = fake.name
    username = factory.Sequence(lambda n: f"{fake.user_name()}-{n}")
    password = factory.PostGenerationMethodCall('set_password', 'redrum-is-murder-reversed')

    @factory.post_generation
    def exercise_groups(self, create, extracted, **kwargs):
        name = "Default Group: "
        default_group, _ = models.Group.objects.get_or_create(name=name)
        self.exercise_groups.add(default_group)


class ExamInfoFactory(DjangoModelFactory):
    class Meta:
        model = models.ExamInfo

    exam = factory.SubFactory(ExamTypeFactory)
    total_score = 90
    passes_exam = True


class StudentInfoFactory(DjangoModelFactory):
    class Meta:
        model = models.StudentInfo

    student_info = factory.RelatedFactory(
        ExamInfoFactory,
        factory_related_name='student',
    )
    user = factory.SubFactory(UserAccountFactory, role=models.UserAccount.STUDENT)


class TestFactory(DjangoModelFactory):
    class Meta:
        model = models.Test

    name = 'EmptyTest'
    label = 'Empty'
    annotation = factory.Sequence(lambda n: f'Test: {n} This is an annotation')


class SubmissionFactory(DjangoModelFactory):
    class Meta:
        model = models.Submission

    text = factory.Sequence(lambda n: f'test case {n}\nx\nx\nx\ntest case {n}')  # noqa
    type = factory.SubFactory(SubmissionTypeFactory)
    student = factory.SubFactory(StudentInfoFactory)


class FeedbackFactory(DjangoModelFactory):
    class Meta:
        model = models.Feedback

    of_submission = factory.SubFactory(SubmissionTypeFactory)


class FeedbackCommentFactory(DjangoModelFactory):
    class Meta:
        model = models.FeedbackComment

    text = 'Well, this is bad...'
    of_tutor = factory.SubFactory(UserAccountFactory)
    of_feedback = factory.SubFactory(FeedbackFactory)


class TutorSubmissionAssignmentFactory(DjangoModelFactory):
    class Meta:
        model = models.TutorSubmissionAssignment

    submission = factory.SubFactory(SubmissionFactory)
