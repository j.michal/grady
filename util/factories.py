import configparser
from xkcdpass import xkcd_password as xp

from core import models
from core.models import (ExamType, Feedback, StudentInfo, Submission,
                         SubmissionType, UserAccount, Group)

STUDENTS = 'students'
TUTORS = 'tutors'
REVIEWERS = 'reviewers'

PASSWORDS = '.importer_passwords'

words = xp.generate_wordlist(wordfile=xp.locate_wordfile(), min_length=5, max_length=8)


def get_random_password(numwords=4):
    """ Returns a cryptographically random string of specified length """
    return xp.generate_xkcdpassword(words, numwords=numwords, delimiter='-')


def store_password(username, groupname, password):
    storage = configparser.ConfigParser()
    storage.read(PASSWORDS)

    if groupname not in storage:
        storage[groupname] = {}

    storage[groupname][username] = password

    with open(PASSWORDS, 'w') as passwd_file:
        storage.write(passwd_file)


class GradyUserFactory:

    def __init__(self,
                 make_password=get_random_password,
                 password_storge=store_password,
                 *args, **kwargs):
        self.make_password = make_password
        self.password_storge = password_storge

    def _get_random_name(self, prefix='', suffix='', k=4):
        return ''.join((prefix, self.make_password(k), suffix))

    def _get_group_for_user_role(self, role):
        """ Returns the groupname for a role """
        return {
            'Student': 'students',
            'Tutor': 'tutors',
            'Reviewer': 'reviewers'
        }[role]

    def _make_base_user(self, username, role, password=None,
                        store_pw=False, fullname='', exam=None, exercise_groups=None, **kwargs):
        """ This is a specific wrapper for the django update_or_create method of
        objects.
            * If now username is passed, a generic one will be generated
            * A new user is created and password and role are set accordingly
            * If the user was there before password IS changed
            * A user must only have one role.

        Returns:
            (User object, str): The user object that was added to the role and
            the password of that user if it was created.
        """
        if not username:
            username = self._get_random_name(prefix=role.lower() + '_')

        username = username.strip()

        user, created = UserAccount.objects.update_or_create(
            username=username,
            fullname=fullname,
            role=role,
            defaults=kwargs)

        if exercise_groups is None:
            exercise_groups = []

        groups_in_db = []
        for group in exercise_groups:
            groups_in_db.append(Group.objects.get_or_create(name=group, exam=exam)[0].pk)

        user.set_groups(groups_in_db)

        if created or password is not None:
            password = self.make_password() if password is None else password
            user.set_password(password)
            user.save()

        if created and store_pw:
            self.password_storge(
                username,
                self._get_group_for_user_role(role),
                password)

        return user

    def make_student(self, username=None, identifier=None,
                     exam=None, submissions=None, **kwargs):
        """ Creates a student. Defaults can be passed via kwargs like in
        relation managers objects.update method. """
        user = self._make_base_user(username, 'Student', exam=exam, **kwargs)
        student_info = StudentInfo.objects.get_or_create(user=user)[0]
        student_info.add_exam(exam)
        if identifier:
            student_info.matrikel_no = identifier
        student_info.save()
        return user

    def make_tutor(self, username=None, exam=None, **kwargs):
        """ Creates or updates a tutor if needed with defaults """
        return self._make_base_user(username, 'Tutor', exam=exam, **kwargs)

    def make_reviewer(self, username=None, exam=None, **kwargs):
        """ Creates or updates a reviewer if needed with defaults """
        return self._make_base_user(username, 'Reviewer', exam=exam, **kwargs)


def make_exams(exams=None, **kwargs):
    if exams is None:
        exams = []

    return [ExamType.objects.get_or_create(
        module_reference=exam['module_reference'],
        defaults=exam)[0] for exam in exams]


def make_groups(groups=None, **kwargs):
    if groups is None:
        groups = []

    return [Group.objects.get_or_create(
        name=group['name'],
        exam=group['exam'])[0] for group in groups]


def make_submission_types(type_id=None, submission_types=[], **kwargs):
    return [SubmissionType.objects.get_or_create(
        name=submission_type['name'], exam_type=type_id,
            defaults=submission_type)[0]
            for submission_type in submission_types]


def make_students(students=None, **kwargs):
    if students is None:
        students = []

    return [GradyUserFactory().make_student(
        username=student['username'],
        exam=ExamType.objects.get(
            module_reference=student['exam']) if 'exam' in student else None,
        password=student.get('password'),
        exercise_groups=student.get('exercise_groups')
    ) for student in students]


def make_tutors(tutors=None, **kwargs):
    if tutors is None:
        tutors = []

    return [GradyUserFactory().make_tutor(**tutor)
            for tutor in tutors]


def make_reviewers(reviewers=None, **kwargs):
    if reviewers is None:
        reviewers = []

    return [GradyUserFactory().make_reviewer(**reviewer)
            for reviewer in reviewers]


def make_feedback(feedback, submission_object):
    feedback_obj = Feedback.objects.update_or_create(
        of_submission=submission_object,
        score=feedback['score'],
        is_final=feedback.get('is_final', False)
    )[0]
    for line_index, comment_list in feedback['feedback_lines'].items():
        for comment in comment_list:
            tutor = models.UserAccount.objects.get(
                username=comment.pop('of_tutor'))
            ret, c = models.FeedbackComment.objects.update_or_create(
                of_line=line_index,
                of_feedback=feedback_obj,
                of_tutor=tutor,
                defaults=comment
            )


def make_submissions(type_id=None, submissions=[], **kwargs):
    submission_objects = []
    for submission in submissions:
        submission_type, _ = SubmissionType.objects.get_or_create(
            name=submission.get('type', 'Auto generated type'), exam_type=type_id)
        student, _ = StudentInfo.objects.get_or_create(
            user=UserAccount.objects.get(
                username=submission.get('user', 'default_user')))
        submission_object, _ = Submission.objects.get_or_create(
            type=submission_type, student=student, defaults={
                'seen_by_student': submission.get('seen_by_student', False),
                'text': submission.get('text', ''),
            })
        if 'feedback' in submission:
            make_feedback(submission['feedback'], submission_object)
        submission_objects.append(submission_object)
    return submission_objects


def make_test_data(data_dict):
    type_id = (make_exams(**data_dict))[0].exam_type_id
    return {
        'exams': make_exams(**data_dict),
        'submission_types': make_submission_types(type_id, **data_dict),
        'students': make_students(**data_dict),
        'tutors': make_tutors(**data_dict),
        'reviewers': make_reviewers(**data_dict),
        'submissions': make_submissions(type_id, **data_dict)
    }


def init_test_instance():
    return make_test_data(
        data_dict={
            'exams': [{
                'module_reference': 'Test Exam 01',
                'total_score': 100,
                'pass_score': 60,
            }],
            'submission_types': [
                {
                    'name': '01. Sort this or that',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!'
                },
                {
                    'name': '02. Merge this or that or maybe even this',
                    'full_score': 35,
                    'description': 'Very complicated',
                    'solution': 'Trivial!'
                },
                {
                    'name': '03. This one exists for the sole purpose to test',
                    'full_score': 30,
                    'description': 'Very complicated',
                    'solution': 'Trivial!'
                }
            ],
            'students': [
                {
                    'username': 'student01',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student02',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student03',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student04',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student05',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student06',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
                {
                    'username': 'student07',
                    'exam': 'Test Exam 01',
                    'password': 'p'
                },
            ],
            'tutors': [
                {
                    'username': 'tutor01',
                    'password': 'p'
                },
                {
                    'username': 'tutor02',
                    'password': 'p'
                }
            ],
            'reviewers': [{
                'username': 'reviewer01',
                'password': 'p'
            }],
            'submissions': [
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student01',
                    'feedback': {
                        'score': 5,
                        'is_final': True,
                        'feedback_lines': {
                            '1': [
                                {
                                    'text': 'This is very bad!',
                                    'of_tutor': 'tutor01'
                                },
                                {
                                    'text': 'I agree',
                                    'of_tutor': 'tutor02'
                                }
                            ],
                            '3': [
                                {
                                    'text': 'Even worse!',
                                    'of_tutor': 'tutor02'
                                }
                            ]
                        }
                    }
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student01',
                    'feedback': {
                        'score': 5,
                        'is_final': True,
                        'feedback_lines': {
                            '1': [
                                {
                                    'text': 'This is very bad!',
                                    'of_tutor': 'tutor01'
                                },
                                {
                                    'text': 'I agree',
                                    'of_tutor': 'tutor02'
                                }
                            ],
                            '3': [
                                {
                                    'text': 'Even worse!',
                                    'of_tutor': 'tutor02'
                                }
                            ]
                        }
                    }
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '03. This one exists for the sole purpose to test',
                    'user': 'student01',
                    'feedback': {
                        'score': 5,
                        'is_final': True,
                        'feedback_lines': {
                            '1': [
                                {
                                    'text': 'This is very bad!',
                                    'of_tutor': 'tutor01'
                                },
                                {
                                    'text': 'I agree',
                                    'of_tutor': 'tutor02'
                                }
                            ],
                            '3': [
                                {
                                    'text': 'Even worse!',
                                    'of_tutor': 'tutor02'
                                }
                            ]
                        }
                    }
                },

                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student02',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student02',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '03. This one exists for the sole purpose to test',
                    'user': 'student02',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student03',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student03',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '03. This one exists for the sole purpose to test',
                    'user': 'student03',
                },

                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student04',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student04',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '03. This one exists for the sole purpose to test',
                    'user': 'student04',
                },

                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '01. Sort this or that',
                    'user': 'student05',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '02. Merge this or that or maybe even this',
                    'user': 'student05',
                },
                {
                    'text': 'function blabl\n'
                            '   on multi lines\n'
                            '       for blabla in bla:\n'
                            '   arrrgh\n'
                            '       asasxasx\n'
                            '           lorem ipsum und so\n',
                    'type': '03. This one exists for the sole purpose to test',
                    'user': 'student05',
                },
            ]}
    )
