from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import TemplateView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('core.urls')),
    path('api/get-token/', obtain_jwt_token),
    path('api/refresh-token/', refresh_jwt_token),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
    path('', TemplateView.as_view(template_name='index.html')),
    path('silk/', include('silk.urls', namespace='silk')),
]
