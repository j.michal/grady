import secrets
import string

from .default import REST_FRAMEWORK

""" A live configuration for enhanced security """
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Read a new SECRET_KEY or generate a new one
SECRET_FILE = 'secret'
try:
    SECRET_KEY = open(SECRET_FILE).read().strip()
    if len(SECRET_KEY) == 0:
        raise Exception
except (IOError, Exception):
    try:
        SECRET_KEY = ''.join(secrets.choice(string.printable)
                             for i in range(50))
        with open(SECRET_FILE, 'w') as secret:
            secret.write(SECRET_KEY)
    except IOError:
        Exception('Please create a %s file with random characters \
        to generate your secret key!' % SECRET_FILE)

# adjust this setting to your needs
ALLOWED_HOSTS = [ 'localhost', '.informatik.uni-goettingen.de' ]

# sample postgres sql database configuration
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'}
]

REST_FRAMEWORK = {
    **REST_FRAMEWORK,
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '300/day',
    }
}
