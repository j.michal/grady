from .default import *
from .live import *

REST_FRAMEWORK['DEFAULT_THROTTLE_RATES']['anon'] = '1000/minute'
