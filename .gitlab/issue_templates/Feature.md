### Description / Overview

### Use cases

(Who is this for?)

### Links / references

### Feature checklist

(Make sure these are completed before closing the issue, with a link to the
relevant commit.)

/label ~"Feature proposal"
