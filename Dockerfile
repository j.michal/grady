FROM node:fermium as node

WORKDIR /app/
COPY frontend/package.json .
COPY frontend/yarn.lock .
RUN yarn

# CACHED
COPY frontend/ .
RUN yarn build

FROM alpine:edge

WORKDIR /code

# This set is needed otherwise the postgres driver wont work
RUN apk update \
    && apk add build-base gcc curl libzmq musl-dev zeromq-dev python3 python3-dev py3-pip \
    && apk add --no-cache postgresql-dev git

# Create symlink for python
RUN ln -sf python3 /usr/bin/python

RUN pip install pipenv
COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv install  --system --deploy && rm -rf /root/.cache

# CACHED
COPY . .
COPY --from=node /app/dist /code/frontend/dist
COPY --from=node /app/dist/index.html /code/core/templates/index.html

ENV PYTHONUNBUFFERED 1
RUN python util/format_index.py
RUN python manage.py collectstatic --noinput

# Reduces image size
RUN apk del build-base musl-dev python3-dev zeromq-dev

CMD ["./deploy.sh"]
